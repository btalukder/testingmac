package com.hpp.puckbandit;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationAvtivity extends AppCompatActivity {

    private UserRegistrationTask mAuthTask = null;
    private EditText mUserNameView;
    private EditText mPasswordView;
    private EditText mEmailView;
    private EditText mPasswordConfirmView;
    private View mProgressView;
    private View mLoginFormView;
    private GlobalVariableStore gvs;
    private  CheckBox showPass;
    private static Boolean successReg=false;
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_avtivity);

        this.gvs = (GlobalVariableStore) getApplicationContext();
        Button registerBtn = (Button) findViewById(R.id.btnRegister);
        mUserNameView = (EditText)findViewById(R.id.reg_fullname);
        mPasswordView = (EditText)findViewById(R.id.reg_password);
        mEmailView = (EditText)findViewById(R.id.reg_email);
        mLoginFormView = findViewById(R.id.registerForm);
        showPass = (CheckBox)findViewById(R.id.passwordShow);
        showPass.setEnabled(false);
        ((TextView)findViewById(R.id.requestFocus)).requestFocus();
        mPasswordConfirmView = (EditText) findViewById(R.id.reg_passwordConfirm);
        mPasswordConfirmView.setEnabled(false);
        mProgressView = findViewById(R.id.reg_progress);
        mPasswordView.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0) {
                    mPasswordConfirmView.setEnabled(true);
                    showPass.setEnabled(true);
                }
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUserNameView.setError(null);
        mPasswordView.setError(null);
        mEmailView.setError(null);
        mPasswordConfirmView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUserNameView.getText().toString();
        String password = mPasswordView.getText().toString();
        String email = mEmailView.getText().toString();


        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {

            mPasswordView.setError(getString(R.string.error_field_required));

            focusView = mUserNameView;
            cancel = true;
        }
        if ( TextUtils.isEmpty(username))
        {
            mUserNameView.setError(getString(R.string.error_field_required));
            focusView = mUserNameView;
            cancel = true;
        }
        if ( TextUtils.isEmpty(email))
        {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            if (cancel == false) {
                mPasswordView.setError(getString(R.string.error_invalid_passwordReg));
                focusView = mPasswordView;
                cancel = true;
            }
        }

        // Check for a valid email address.
         if (!TextUtils.isEmpty(username) && !isUsernameValid(username)) {
            mUserNameView.setError(getString(R.string.error_invalid_userNameReg));
            focusView = mUserNameView;
            cancel = true;
        }
        if (!TextUtils.isEmpty(email) && !isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_emailReg));
            focusView = mEmailView;
            cancel = true;
        }
        if (!TextUtils.isEmpty(password))
        {
            String confirmPassword = mPasswordConfirmView.getText().toString();
            if (TextUtils.isEmpty(confirmPassword)) {
                mPasswordConfirmView.setError(getString(R.string.error_field_required));
                if (cancel == false)
                    focusView = mPasswordConfirmView;
                cancel = true;
            }
            else
            {
              if (confirmPassword.equals(password) == false)
              {
                  mPasswordConfirmView.setError(getString(R.string.error_invalid_passwordConfirmPasswrd));
                  focusView = mPasswordConfirmView;
                  cancel = true;
              }
            }
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            mAuthTask = new UserRegistrationTask(username, password,email);

            mAuthTask.execute(username, password,email);
//
        }
    }


    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        if (checked) {
            // Check which checkbox was clicked
            mPasswordConfirmView.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }
        else
        {
            mPasswordConfirmView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            mPasswordConfirmView.setSelection(mPasswordConfirmView.getText().length());
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
         Pattern pattern;
         Matcher matcher;
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();

    }
    private boolean isUsernameValid(String username) {
        if (username.length() < 5 && username.length() > 15 )
            return false;
        else
            return  true;
    }

    private boolean isPasswordValid(String password) {

        if (password.length() < 6 )
        {

            return false;
        }
        boolean hasUppercase = !password.equals(password.toLowerCase());
        boolean hasLowercase = !password.equals(password.toUpperCase());

        if (hasUppercase== false || hasLowercase == false)
        {
            return false;
        }
        boolean hasSpecial   = !password.matches("[A-Za-z0-9 ]*");
        if (hasSpecial == true)
        {
            return false;
        }
        return true;
    }
    public class UserRegistrationTask extends AsyncTask<String, Void, String> {

        private final String mUsername;
        private final String mPassword;
        private final String mEmailAdd;

        UserRegistrationTask(String username, String password,String mEmail) {
            mUsername = username;
            mPassword = password;
            mEmailAdd = mEmail;
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO: attempt authentication against a network service.
            String add_prod_url = gvs.getServerIP()+"/register.php";
            String username = params[0];
            String password= params[1];
            String email= params[2];

            try {
                // Simulate network access.
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                return null;
            }

            try {



                String data = "usernameV="+ URLEncoder.encode(username,"UTF-8")+"&"+
                        "passworV="+URLEncoder.encode(password,"UTF-8")+"&"+
                        "emailV="+URLEncoder.encode(email,"UTF-8");
                System.out.println(data);
                URL url = new URL(add_prod_url);
                HttpURLConnection http= (HttpURLConnection)url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setFixedLengthStreamingMode(data.getBytes().length);
                http.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
                PrintWriter out = new PrintWriter(http.getOutputStream());
                out.print(data);
                out.close();

                InputStream is = http.getInputStream();
                String output = getStringFromInputStream(is);
                is.close();

                System.out.println("Output: "+output);
                return output;


            }catch (MalformedURLException e)
            {
                Log.i("Register", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.i("Register", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            }
//            for (String credential : DUMMY_CREDENTIALS) {
//                String[] pieces = credential.split(":");
//                if (pieces[0].equals(mUsername)) {
//                    // Account exists, return true if the password matches.
//                    return pieces[1].equals(mPassword);
//                }
//            }

            // TODO: register the new account here.
            return "-1";
        }


        private  String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return sb.toString();

        }

        @Override
        protected void onPostExecute(final String success) {
            mAuthTask = null;

            showProgress(false);

            if (success.contains("sucessReg")) {
                // username password match
                /*SharedPreferences settings = getSharedPreferences(LoginActivity.PREFS_NAME, 0); // 0 - for private mode
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("hasLoggedIn", true);
                editor.putString("username", mUserNameView.getText().toString());
                editor.commit();*/


                finishLogin(success.split(":")[1]);

            } else {
                mUserNameView.setError(getString(R.string.error_invalid_userNameRegUnique) + "  for example : " + success );
                mUserNameView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
    void finishLogin(String id)
    {
        // TODO need to provide proper homescreen class
        gvs.setUserid(id);
        Intent intent  = new Intent(this,MyCustomDrawerLayout.class);
        intent.putExtra("username", mUserNameView.getText().toString());
        startActivity(intent);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
    public void RedirToLogin(View v) {
        // TODO proper page redir
        Intent intent  = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }
}
