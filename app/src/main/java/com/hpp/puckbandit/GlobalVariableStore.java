package com.hpp.puckbandit;

import android.app.Application;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ankush_PC on 04-06-2016.
 */
public class GlobalVariableStore extends Application {
    private static String homeTeam;
    private static String visitingTeam;
    private static String myPick;

    public static String getServerIP() {
        return serverIP;
    }

    public static void setServerIP(String serverIP) {
        GlobalVariableStore.serverIP = serverIP;
    }
    //private static String serverIP="http://10.0.2.2:82";
     private static String serverIP="http://50.112.166.85:80";
    private static HashMap<String,ArrayList<String>> fantasyPlayersMap;
    private String userid;

    public static HashMap<String, ArrayList<String>> getFantasyPlayersMap() {
        return fantasyPlayersMap;
    }

    public static void setFantasyPlayersMap(HashMap<String, ArrayList<String>> fantasyPlayersMap) {
        GlobalVariableStore.fantasyPlayersMap = fantasyPlayersMap;
    }


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }




    public static String getCurrentGameId() {
        return currentGameId;
    }

    public static void setCurrentGameId(String currentGameId) {
        GlobalVariableStore.currentGameId = currentGameId;
    }

    private static String currentGameId;


    public static final HashMap<String,String> threetoFullNameMap;
    static
    {
        threetoFullNameMap =  new HashMap<>();
        threetoFullNameMap.put("CGY" ,"Calgary");
        threetoFullNameMap.put("EDM" ,"Edmonton");
        threetoFullNameMap.put("MTL" ,"Montreal");
        threetoFullNameMap.put("OTT" ,"Ottawa");
        threetoFullNameMap.put("TOR" ,"Toronto");
        threetoFullNameMap.put("VAN" ,"Vancouver");
        threetoFullNameMap.put("WPG","Winnipeg");
    }

    public static final HashMap<String,String> fulltoThreeNameMap;
    static
    {
        fulltoThreeNameMap =  new HashMap<>();
        fulltoThreeNameMap.put("calgary","CGY");
        fulltoThreeNameMap.put("edmonton","EDM" );
        fulltoThreeNameMap.put("montreal","MTL" );
        fulltoThreeNameMap.put("ottawa","OTT");
        fulltoThreeNameMap.put("toronto","TOR");
        fulltoThreeNameMap.put("vancouver","VAN");
        fulltoThreeNameMap.put("winnipeg","WPG");
    }
    public static String getFavrtteam() {
        return favrtteam;
    }

    public static void setFavrtteam(String favrtteam) {
        GlobalVariableStore.favrtteam = favrtteam;
    }

    private static  String favrtteam;

    public static String getHomeTeam() {
        return homeTeam;
    }

    public static void setHomeTeam(String homeTeam) {
        GlobalVariableStore.homeTeam = homeTeam;
    }

    public static String getVisitingTeam() {
        return visitingTeam;
    }

    public static void setVisitingTeam(String visitingTeam) {
        GlobalVariableStore.visitingTeam = visitingTeam;
    }

    public static String getMyPick() {
        return myPick;
    }

    public static void setMyPick(String myPick) {


        GlobalVariableStore.myPick = fulltoThreeNameMap.get(myPick.toLowerCase());
    }
}
