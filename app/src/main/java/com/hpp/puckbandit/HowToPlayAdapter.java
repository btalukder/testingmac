package com.hpp.puckbandit;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by USER on 28-05-2016.
 */
public class HowToPlayAdapter extends BaseExpandableListAdapter {

    private Context ctx;
    private HashMap <String ,List<String>> howToplaySteps ;
    private List<String> data;
    public HowToPlayAdapter(Context ctx , HashMap<String,List<String>> steps, List<String> data)
    {
        this.ctx = ctx;
        this.howToplaySteps=steps;
        this.data = data;

    }
    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return howToplaySteps.get(data.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return howToplaySteps.get(data.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String groupTitle =(String) getGroup(groupPosition);
        groupTitle=groupTitle.substring(2);
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.howtoplay_parent,parent,false);
        }
        TextView parentText = (TextView) convertView.findViewById(R.id.howtoplayParent);
        parentText.setText(groupTitle);
        parentText.setTypeface(null, Typeface.BOLD);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        String child = (String)getChild(groupPosition,childPosition);
        child =child.substring(2);
        if (convertView == null )
        {
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.howtoplay_child,parent,false);

        }
        TextView child_text = (TextView)convertView.findViewById(R.id.howtoplayChild);
        child_text.setText(child);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
