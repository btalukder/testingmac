package com.hpp.puckbandit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by USER on 30-05-2016.
 */
public class TeamsDataProvider {

    public static HashMap<String,List<String>> getInfo()
    {
        HashMap<String ,List<String>> parentMenu = new HashMap<String, List<String>>();
        List<String> menus ;
        List<String> items = new ArrayList<>();

        items.add("Montreal Canadiens");
        items.add("Ottawa Senators");
        items.add("Calgary Flames");
        items.add("Edmonton Oilers");
        items.add("Toronto Maple Leafs");
        items.add("Winnipeg Jets");
        items.add("Winnipeg Jets");
        items.add("Vancouver Cancuks");

        parentMenu.put("Teams" , items);
        return  parentMenu;

    }
}
