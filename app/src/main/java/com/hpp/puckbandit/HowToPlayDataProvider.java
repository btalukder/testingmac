package com.hpp.puckbandit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by USER on 28-05-2016.
 */
public class HowToPlayDataProvider {

    public static HashMap<String,List<String>> getInfo()
    {
        HashMap<String,List<String> > details = new HashMap<>();
        List<String> firstInfo = new ArrayList<>();
        firstInfo.add(0,"study the stats of the games care fully");

        List<String> seconfInfo = new ArrayList<>();
        seconfInfo.add("make a pick properly");

        List<String> thirdInfo = new ArrayList<>();
        thirdInfo.add("Run the simulator properly");

        List<String> fourthInfo = new ArrayList<>();
        fourthInfo.add("how To earn Points");

        List<String> fifthInfo = new ArrayList<>();
        fifthInfo.add("TBD");

        List<String> sixthInfo = new ArrayList<>();
        sixthInfo.add("TBD");

        details.put("1 Study the stats".toUpperCase() , firstInfo);
        details.put("2 Make a Pick".toUpperCase() , seconfInfo);
        details.put("3 Run the simulation".toUpperCase() , thirdInfo);
        details.put("4 How to earn points".toUpperCase() , fourthInfo);
        details.put("5 Privacy policy".toUpperCase() , fifthInfo);
        details.put("6 TERMS OF USE".toUpperCase() , sixthInfo);

        return details;
    }

}
