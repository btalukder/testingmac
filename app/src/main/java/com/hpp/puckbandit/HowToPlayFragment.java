package com.hpp.puckbandit;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ankush_PC on 04-06-2016.
 */
public class HowToPlayFragment extends Fragment{

    View rootView;

    HashMap<String , List<String >> studyTheStats;
    ExpandableListView studyStatsView;
    HowToPlayAdapter adapter;
    List<String> data;
    public HowToPlayFragment(){}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
try{
        this.rootView = inflater.inflate(R.layout.activity_how_to_play, container, false);


    ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("How To Play");
    studyStatsView = (ExpandableListView)rootView.findViewById(R.id.howtoplay1st);
    studyTheStats = HowToPlayDataProvider.getInfo();
    data = new ArrayList<String>(studyTheStats.keySet());
    Collections.sort(data, new Comparator<String>() {

        public int compare(String o1, String o2) {
            //int index1 = Integer.parseInt(o1.split(":")[0].substring(o1.split(":")[0].length()-1));
            //int index2 = Integer.parseInt(o2.split(":")[0].substring(o2.split(":")[0].length()-1));
            int index1 = o1.charAt(0)- '0';
            int index2 = o2.charAt(0)-'0';
            return index1 - index2;
        }
    });
    adapter = new HowToPlayAdapter(getActivity(),studyTheStats,data);
    studyStatsView.setAdapter(adapter);

    return rootView;

}catch (Exception e)
        {
            e.printStackTrace();
        }






        return rootView;
    }

}
