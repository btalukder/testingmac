package com.hpp.puckbandit;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Ankush_PC on 01-06-2016.
 */
public class GameSummaryActivity extends AppCompatActivity {

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ListView mDrawerList2;
    private static GlobalVariableStore gvs;
    private ArrayAdapter<String> mAdapter2;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private String jsonString;
    public static final HashMap<String,String> teamNameMap;
    static
    {
        teamNameMap =  new HashMap<>();
        teamNameMap.put("calgary","CGY" );
        teamNameMap.put("edmonton","EDM");
        teamNameMap.put("montreal","MTL");
        teamNameMap.put("ottawa","OTT" );
        teamNameMap.put("toronto","TOR");
        teamNameMap.put("vancouver","VAN" );
        teamNameMap.put("winnipeg","WPG");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_main);

        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout1);
        //mActivityTitle = getTitle().toString();

        gvs=(GlobalVariableStore)getApplicationContext();
        mActivityTitle="Game Summary";
        getSupportActionBar().setTitle(mActivityTitle);
        mDrawerList2 = (ListView)findViewById(R.id.navList2);

        addDrawerItems();
        //addDrawerItemssecond(this);
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeButtonEnabled(true);

        expListView = (ExpandableListView) findViewById(R.id.expdlist);


        //**Taking Json String from intent **//
        Bundle bundle = getIntent().getExtras();
         jsonString = bundle.getString("json");


        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                /*Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();*/
                mDrawerLayout.closeDrawers();
                loadFragment("teams_"+listDataChild.get(
                        listDataHeader.get(groupPosition)).get(
                        childPosition));
                return false;
            }
        });


        loadGameSummaryFragment();


    }

    private void loadGameSummaryFragment(){
        Fragment fragment=null;
        mActivityTitle="Game Summary";
        //getSupportActionBar().setTitle("Scheduled Game");
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        fragment = new GameSummaryFragment(jsonString);
        ft.replace(R.id.frame, fragment);
        ft.addToBackStack("GameSummaryFragment");
        ft.commit();

    }

    private void addDrawerItems() {
        final String[] osArray = { "Home", "Game Summary","Leaderboard", "Reset","Logout"};
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               /* Toast.makeText(MyCustomDrawerLayout.this, "Time for an upgrade!", Toast.LENGTH_SHORT).show();*/
                mDrawerLayout.closeDrawers();

                if(osArray[position].equalsIgnoreCase("Reset")){

                    resetMethod();
                }
                else {
                    loadFragment(osArray[position]);
                }
            }
        });
    }

    private void addDrawerItemssecond(final Context ctx) {
        final String[] osArray2 = { "Leaderboard", "Reset","Logout"};
        mAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray2);
        mDrawerList2.setAdapter(mAdapter2   );

        mDrawerList2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(MyCustomDrawerLayout.this, "Time for an upgrade!", Toast.LENGTH_SHORT).show();


                if(osArray2[position].equalsIgnoreCase("logout")){

                    //clear all shared preferences and sending to login activity
                    SharedPreferences preferences = getSharedPreferences(LoginActivity.PREFS_NAME, 0);

                    preferences.edit().remove("hasLoggedIn").commit();
                    mDrawerLayout.closeDrawers();

                    Intent intent  = new Intent(ctx ,LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                }
                else {
                    mDrawerLayout.closeDrawers();
                    loadFragment(osArray2[position]);
                }
            }
        });
    }


    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings1) {
            return true;
        }

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

public void loadFragment(String fragmentName){

    Fragment fragment=null;



    // Add the fragment to the activity, pushing this transaction
    // on to the back stack.
    FragmentTransaction ft = getFragmentManager().beginTransaction();
    /*ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
            R.animator.fragment_slide_left_exit,
            R.animator.fragment_slide_right_enter,
            R.animator.fragment_slide_right_exit);*/
    if(fragmentName.equalsIgnoreCase("home")){
        Intent intent  = new Intent(this,MyCustomDrawerLayout.class);
        startActivity(intent);
    }
    if(fragmentName.equalsIgnoreCase("game summary")){
        //getSupportActionBar().setTitle("Scheduled Game");
        mActivityTitle="Game Summary";
                fragment = new GameSummaryFragment(jsonString);
                ft.replace(R.id.frame, fragment);
                ft.addToBackStack("GameSummaryFragment");
                ft.commit();

    }
    else if(fragmentName.equalsIgnoreCase("standings")){
        fragment = new StandingsFragment();
            mActivityTitle="Standings";
 //       getSupportActionBar().setTitle("Standings");
        ft.replace(R.id.frame, fragment);
        ft.addToBackStack("StandingsFragemt");
        ft.commit();
    }
    else if(fragmentName.equalsIgnoreCase("leaderboard")){
        fragment = new LeaderBoardFragment();
        //getSupportActionBar().setTitle("Leaderboard");
        mActivityTitle="Leaderboard";
        ft.replace(R.id.frame, fragment);
        ft.addToBackStack("LeaderBoardFragemt");
        ft.commit();
    }

}
    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
    //**Reset Activity**//

    private void resetMethod()
    {
        new AlertDialog.Builder(this)
                .setTitle("Reset")
                .setMessage("Do you really want to reset all your game data? All previous data will be lost")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        resetData();
                    }})
                .setNegativeButton(android.R.string.no, null).show();

    }
    private void resetData()
    {
        DatabaseOperations resetDb = new DatabaseOperations(this,gvs);
        resetDb.resetAll(resetDb,gvs.getUserid());
        Toast.makeText(getBaseContext(), "All Data has been reset.",
                Toast.LENGTH_SHORT).show();
    }

}

