package com.hpp.puckbandit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ankush on 19/06/16.
 */
public class NewsFeedAdapter extends BaseAdapter{



        private static final int TYPE_NEWS_INFO = 0;
        private static final int TYPE_MAX_COUNT = 1;
        private String scorer="" ;
        private String assist="";
        private String scoreline="";
        private String scoringteam="";
        private String eventTime="";
        private String goalType="";

        private ArrayList<String> arrayList = new ArrayList();
    private ArrayList<String> arrayListHeader = new ArrayList();
        private int type=0;
        private String headerText;
        public String colorCode;

        private LayoutInflater mInflater;

        private HashMap<Integer , Integer> typeHM = new HashMap<>();

        public NewsFeedAdapter(MyCustomDrawerLayout activity) {
            mInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void addNewsItem(final String text, final String typetext,final String headerText) {
            // mData.add(item);
            // this.arrayListHeader.p=headerText;
            switch(typetext){
                case "newsinfo":
                    this.type = 0;
                    arrayListHeader.add(headerText);
                    typeHM.put(arrayList.size(),this.type);
                    arrayList.add(text);
                    break;
                default:
                    break;

            }


            //notifyDataSetChanged();
        }


        public void addSeparatorItem(final String item) {
           /* mData.add(item);
            // save separator position
            mSeparatorsSet.add(mData.size() - 1);
            notifyDataSetChanged();*/
        }

        @Override
        public int getItemViewType(int position) {

            //return this.type;
            return typeHM.get(position);
            // return mSeparatorsSet.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
        }

        @Override
        public int getViewTypeCount() {
            return TYPE_MAX_COUNT;
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public String getItem(int position) {
            return arrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            int type = getItemViewType(position);

            System.out.println("getView " + position + " " + convertView + " type = " + type);

            switch (type) {
                case TYPE_NEWS_INFO:
                    if (convertView == null) {
                        holder = new ViewHolder();
                        convertView = mInflater.inflate(R.layout.newsinfo, null);
                        holder.headerText = (TextView) convertView.findViewById(R.id.header_NF);
                        holder.contentText=(TextView) convertView.findViewById(R.id.content_NF);
                        convertView.setTag(holder);
                    }
                    else{
                        holder = (ViewHolder)convertView.getTag();

                    }

                    holder.headerText.setText(arrayListHeader.get(position));
                    holder.contentText.setText(arrayList.get(position));
                        // holder.publicText.setText( this.currentText);
                    break;

            }


            /*else {
                   holder = (ViewHolder)convertView.getTag();


                if(holder.publicText!=null)
                 System.out.println("IN ELSE "+holder.publicText.getText());
            }*/
            //holder.textView.setText(mData.get(position));
            return convertView;
        }


    public static class ViewHolder {
        public TextView headerText;
        public TextView contentText;
        public ImageView image;
    }


    }



