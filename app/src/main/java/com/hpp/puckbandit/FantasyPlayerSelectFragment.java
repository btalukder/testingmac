package com.hpp.puckbandit;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Ankush_PC on 03-06-2016.
 */
public class FantasyPlayerSelectFragment extends Fragment implements View.OnClickListener {

    String teamNameToShow;
    HashMap<Integer,String> teamPlayerNames= new HashMap<Integer,String>();
    public static final HashMap<String,String> teamNameMap;
    private static GlobalVariableStore gvs;
    private ArrayList<String> fantasyPlayerSelected ;
    static
    {
        teamNameMap =  new HashMap<>();
        teamNameMap.put("CGY" ,"Calgary");
        teamNameMap.put("EDM" ,"Edmonton");
        teamNameMap.put("MTL" ,"Montreal");
        teamNameMap.put("OTT" ,"Ottawa");
        teamNameMap.put("TOR" ,"Toronto");
        teamNameMap.put("VAN" ,"Vancouver");
        teamNameMap.put("WPG","Winnipeg");
    }
    private  TextView tempTeamPlayer;
    private HashMap<String,ArrayList<String>> hmtmp;
    public FantasyPlayerSelectFragment() {
    }
    public FantasyPlayerSelectFragment(String s) {
        this.teamNameToShow=s;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        gvs=(GlobalVariableStore)getActivity().getApplicationContext();
        View rootView = inflater.inflate(R.layout.fantasy_select_fragment, container, false);

        fantasyPlayerSelected = new ArrayList<String>();
        hmtmp=gvs.getFantasyPlayersMap();

       // ArrayList<String> altemp=new ArrayList<String>();
        if(hmtmp==null)
            hmtmp=new HashMap<String,ArrayList<String>>();
        if(hmtmp.containsKey(teamNameToShow)){

            //**Populate background with gray color**//
        //    altemp=hmtmp.get(teamNameToShow);
            fantasyPlayerSelected=hmtmp.get(teamNameToShow);
        }
        else{
            //do nothing
        }

        RosterInfo getRosterdata = new RosterInfo();
        try {
            String result = getRosterdata.execute(teamNameToShow).get();
            System.out.println(result);
            parseJsonAndPopulate(result);

            for(int i =1;i<=18;i++)
            {
                String viewId="player"+i;

                int id = getResources().getIdentifier(viewId,"id",getActivity().getPackageName());
                tempTeamPlayer = (TextView)rootView.findViewById(id);
                tempTeamPlayer.setText((String)teamPlayerNames.get(i));
                if(fantasyPlayerSelected.contains((String)teamPlayerNames.get(i))){
                    tempTeamPlayer.setBackgroundColor(Color.parseColor("#c2c2a3"));
                }
                tempTeamPlayer.setOnClickListener(this);

            }

            Button nextbtn =(Button)rootView.findViewById(R.id.nextbtn_FP);

            nextbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(teamNameToShow.equalsIgnoreCase(gvs.getVisitingTeam())){

                        hmtmp.put(teamNameToShow,fantasyPlayerSelected);
                        gvs.setFantasyPlayersMap(hmtmp);

                        Fragment fragment=null;
                        FragmentTransaction ft = getFragmentManager().beginTransaction();

                        fragment = new FantasyPlayerSelectFragment(gvs.getHomeTeam());
                        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Select Fantasy Players("+gvs.getHomeTeam()+")");
                        ft.replace(R.id.frame, fragment);
                        ft.addToBackStack("HomeFragment");
                        ft.commit();


                    }

                    else{
                        //HashMap<String,ArrayList<String>> hm=gvs.getFantasyPlayersMap();
                        hmtmp.put(teamNameToShow,fantasyPlayerSelected);
                        gvs.setFantasyPlayersMap(hmtmp);

                        //Go To Simulation
                        Intent intent  = new Intent(getActivity(),MainActivityList.class);
                        startActivity(intent);
                    }
                }
            });



        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return rootView;
    }


    void parseJsonAndPopulate(String result)
    {
        try {
            JSONObject json = new JSONObject(result);
            JSONObject jsonTeam = json.getJSONObject(teamNameToShow);

            Iterator<String> iter = jsonTeam.keys();
            int i =0;
            String id;
            for (int count=1; count<=20;count++)
            {
                teamPlayerNames.put(count,(String)jsonTeam.get(count+""));

            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {

     int clickedTextID= view.getId();
           if(view instanceof TextView){
           TextView tmp=(TextView) view.findViewById(clickedTextID);
            String tempText = tmp.getText().toString();



             if(fantasyPlayerSelected.contains(tempText)){
                tmp.setBackgroundColor(Color.parseColor("#ffffff"));
                fantasyPlayerSelected.remove(tempText);
            }

            else if(fantasyPlayerSelected.size()>=3){

                Toast.makeText(getActivity(),"Only Three Players You Can Select",Toast.LENGTH_SHORT).show();
            }
         else{

                tmp.setBackgroundColor(Color.parseColor("#c2c2a3"));
                fantasyPlayerSelected.add(tempText);

            }
        }


    }

    public class RosterInfo extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... params) {
            String add_prod_url = gvs.getServerIP()+"/getrosterinfo.php";
            String teamname = params[0];


            try {

                String data = "usernameV="+ URLEncoder.encode(teamname,"UTF-8");
                URL url = new URL(add_prod_url);
                HttpURLConnection http= (HttpURLConnection)url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setFixedLengthStreamingMode(data.getBytes().length);
                http.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
                PrintWriter out = new PrintWriter(http.getOutputStream());
                out.print(data);
                out.close();

                InputStream is = http.getInputStream();
                String output = getStringFromInputStream(is);
                is.close();

                return output;

            }catch (MalformedURLException e)
            {
                Log.i("rosterinfo", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.i("rosterinfo", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        private  String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return sb.toString();

        }
    }



}



