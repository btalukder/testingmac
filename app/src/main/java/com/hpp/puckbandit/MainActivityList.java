package com.hpp.puckbandit;

/**
 * Created by Ankush_PC on 23-05-2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivityList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private databaseTaskSimulate dbtask ;
    //private MovieAdapter mAdapter;
    private int count=-1;
    private int bgngdisplayCount=0;
    private ImageButton clickme;
    private ArrayList<String> al;
    private ArrayAdapter<String> adapter;
    private ProgressBar pbar;
    private Handler handler = new Handler();
    private int progress=0;
    private MyCustomAdapter mAdapter;
    private ListView prdonelview;
    private HashMap<Integer,String> eventMap =new HashMap<>();
    private LinearLayout.LayoutParams params1 ;
    private LinearLayout.LayoutParams params2 ;
    private LinearLayout.LayoutParams params3 ;

    String pointsEarned;
    TextView tv1 ;
    TextView tv2 ;
    TextView tv3 ;
    TextView scr1;
    TextView scr2;
    TextView visitingTeam;
    TextView homeTeam;
    private String guestroster,homeroster,favteam,overunder,username,userpick,currgameid;
    private String json=null;
    boolean activeSummaryButton=false;
    private HashMap<Integer,String> secondaryEventMap=new HashMap<>();
    private boolean isOvertime=false;
    HashMap<Integer,String> teamPlayerNames= new HashMap<Integer,String>();
    public static final HashMap<String,String> rosterNameMap;
    static
    {
        rosterNameMap =  new HashMap<>();
        rosterNameMap.put("CGY" ,"calgary");
        rosterNameMap.put("EDM" ,"edmonton");
        rosterNameMap.put("MTL" ,"montreal");
        rosterNameMap.put("OTT" ,"ottawa");
        rosterNameMap.put("TOR" ,"toronto");
        rosterNameMap.put("VAN" ,"vancouver");
        rosterNameMap.put("WPG","winnipeg");
    }
    private static GlobalVariableStore gvs ;
    private LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getSharedPreferences(LoginActivity.PREFS_NAME, 0);
        setContentView(R.layout.simulation_activity);
        gvs = (GlobalVariableStore)getApplicationContext();
        getSupportActionBar().setTitle("Game Simulation");
        username=settings.getString("username","ankushb");
         prdonelview = (ListView)findViewById(R.id.prd1listview);
        tv1=(TextView)findViewById(R.id.prdtxt1);
        tv2=(TextView)findViewById(R.id.prdtxt2);
        tv3=(TextView)findViewById(R.id.prdtxt3);
        params1 =(LinearLayout.LayoutParams)tv1.getLayoutParams();
        params2 =(LinearLayout.LayoutParams)tv2.getLayoutParams();
        params3 =(LinearLayout.LayoutParams)tv3.getLayoutParams();

        scr1=(TextView)findViewById(R.id.scr1);
        scr2=(TextView)findViewById(R.id.scr2);
        visitingTeam=(TextView)findViewById(R.id.tm1);
        homeTeam=(TextView)findViewById(R.id.tm2);

        homeTeam.setText(gvs.getHomeTeam());
        visitingTeam.setText(gvs.getVisitingTeam());
        guestroster=rosterNameMap.get(gvs.getVisitingTeam());
        homeroster=rosterNameMap.get(gvs.getHomeTeam());
        userpick=gvs.getMyPick();
        favteam=(gvs.getFavrtteam().toUpperCase());
        currgameid=gvs.getCurrentGameId();
        System.out.println(guestroster+" "+homeroster+" "+userpick+" "+favteam+" currgameid = "+currgameid);
        /*final ListView prdtwolview = (ListView)findViewById(R.id.prd2listview);
        final ListView prdthreelview = (ListView)findViewById(R.id.prd3listview);
        final ListView ovrtimelview = (ListView)findViewById(R.id.ovrtmlistview);
        *//*final ListView lview = (ListView)findViewById(R.id.listview);
        String[] items = {"hello","world"};
           al = new ArrayList<>(Arrays.asList(items));
        adapter= new ArrayAdapter<String>(this,R.layout.list_item,R.id.listtxt,al);
        lview.setAdapter(adapter);
*/
        mAdapter = new MyCustomAdapter();

        prdonelview.setAdapter(mAdapter);
      /*  prdtwolview.setAdapter(mAdapter);
        prdthreelview.setAdapter(mAdapter);
        ovrtimelview.setAdapter(mAdapter);
*/

        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.circular);
         pbar = (ProgressBar) findViewById(R.id.pbar);
        pbar.setProgress(0);   // Main Progress
        pbar.setSecondaryProgress(60); // Secondary Progress
        pbar.setMax(60); // Maximum Progress
        pbar.setProgressDrawable(drawable);


/*

String a ="{\"Game\":{\"USERNO\":\"1\",\"NO\":\"1\",\"Final\":{\"Edmonton\":{\"Goals\":\"5\",\"FanPlayers\":{\"C. McDavid\":{\"G\":\"0\",\"A\":\"1\"},\"M. Hendricks\":{\"G\":\"0\",\"A\":\"1\"},\"L. Korpikoski\":{\"G\":\"1\",\"A\":\"0\"},\"M. Letestu\":{\"G\":\"2\",\"A\":\"1\"},\"P. Maroon\":{\"G\":\"1\",\"A\":\"1\"},\"J. Eberie\":{\"G\":\"0\",\"A\":\"1\"},\"Z. Kassin\":{\"G\":\"0\",\"A\":\"1\"},\"T. Hall\":{\"G\":\"1\",\"A\":\"0\"}}},\"Ottawa\":{\"Goals\":\"3\",\"FanPlayers\":{\"N. Paul\":{\"G\":\"1\",\"A\":\"0\"},\"Z. Smith\":{\"G\":\"0\",\"A\":\"1\"},\"M. Puempel\":{\"G\":\"1\",\"A\":\"0\"},\"R. Dzingel\":{\"G\":\"0\",\"A\":\"1\"},\"B. Robinson\":{\"G\":\"1\",\"A\":\"0\"},\"M. Kostka\":{\"G\":\"0\",\"A\":\"2\"}}}},\"FanTasyStrength\":\"238\",\"Period1\":{\"1\":[\"4\",\"GOL\",\"Edmonton\",\"4:20\",\"5-on-5\",\"1-0\",\"T. Hall\",\"Z. Kassin\"],\"2\":[\"6\",\"PP2\",\"Ottawa\",\"6:00\"],\"3\":[\"14\",\"PP2\",\"Edmonton\",\"14:00\"],\"4\":[\"15\",\"GOL\",\"Edmonton\",\"15:53\",\"PPG\",\"2-0\",\"L. Korpikoski\",\"C. McDavid\",\"M. Hendricks\"],\"5\":[\"16\",\"PP2\",\"Edmonton\",\"16:02\"],\"6\":[\"18\",\"PP2\",\"Edmonton\",\"18:05\"]},\"Period2\":{\"1\":[\"22\",\"PP2\",\"Edmonton\",\"2:06\"],\"2\":[\"24\",\"GOL\",\"Edmonton\",\"4:28\",\"5-on-5\",\"3-0\",\"M. Letestu\",\"P. Maroon\"],\"3\":[\"24\",\"PP2\",\"Edmonton\",\"4:54\"],\"4\":[\"27\",\"PP2\",\"Edmonton\",\"7:00\"],\"5\":[\"30\",\"GOL\",\"Edmonton\",\"10:42\",\"5-on-5\",\"4-0\",\"P. Maroon\",\"M. Letestu\"],\"6\":[\"32\",\"PP2\",\"Ottawa\",\"12:00\"]},\"Period3\":{\"1\":[\"43\",\"GOL\",\"Ottawa\",\"3:41\",\"5-on-5\",\"4-1\",\"B. Robinson\",\"R. Dzingel\"],\"2\":[\"49\",\"PP2\",\"Ottawa\",\"9:02\"],\"3\":[\"51\",\"GOL\",\"Ottawa\",\"11:17\",\"5-on-5\",\"4-2\",\"N. Paul\",\"Z. Smith\",\"M. Kostka\"],\"4\":[\"54\",\"GOL\",\"Ottawa\",\"14:02\",\"5-on-5\",\"4-3\",\"M. Puempel\",\"M. Kostka\"],\"5\":[\"58\",\"ENT\",\"Ottawa\"],\"6\":[\"59\",\"GOL\",\"Edmonton\",\"19:31\",\" EN\",\"5-3\",\"M. Letestu\",\"J. Eberie\"]}}}";

        a="{\"Game\":{\"USERNO\":\"1\",\"NO\":\"1\",\"Final\":{\"EDM\":{\"Goals\":\"1\",\"FanPlayers\":{\"M. Letestu\":{\"G\":\"1\",\"A\":\"0\"}}},\"TOR\":{\"Goals\":\"2\",\"FanPlayers\":{\"B. Laich\":{\"G\":\"0\",\"A\":\"1\"},\"C. Greening\":{\"G\":\"2\",\"A\":\"0\"},\"T. Lindberg\":{\"G\":\"0\",\"A\":\"1\"},\"L. Komarov\":{\"G\":\"0\",\"A\":\"1\"}}}},\"FanTasyStrength\":\"74\",\"Period1\":{\"1\":[\"10\",\"PP2\",\"TOR\",\"10:05\"],\"2\":[\"18\",\"PP2\",\"TOR\",\"18:00\"]},\"Period2\":{\"1\":[\"20\",\"PP2\",\"EDM\",\"0:01\"],\"2\":[\"21\",\"PP2\",\"TOR\",\"1:06\"],\"3\":[\"22\",\"PP2\",\"TOR\",\"2:00\"],\"4\":[\"23\",\"PP2\",\"EDM\",\"3:04\"],\"5\":[\"30\",\"PP2\",\"TOR\",\"10:07\"],\"6\":[\"31\",\"GOL\",\"TOR\",\"11:15\",\"PPG\",\"0-1\",\"M. Grabner\",\"B. Laich\"],\"7\":[\"34\",\"PP2\",\"EDM\",\"14:04\"],\"8\":[\"38\",\"PP2\",\"EDM\",\"18:06\"],\"9\":[\"39\",\"GOL\",\"EDM\",\"19:26\",\"PPG\",\"1-1\",\"M. Letestu\",\"unassisted\"]},\"Period3\":{\"1\":[\"49\",\"PP2\",\"TOR\",\"9:02\"],\"2\":[\"54\",\"PP2\",\"EDM\",\"14:03\"]},\"Overtime\":[\"61\",\"GOL\",\"TOR\",\"1:15\",\"OTG\",\"1-2\",\"C. Greening\",\"L. Komarov\",\"T. Lindberg\"]}}";
*/


/*

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new MovieAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
*/

 //       prepareMovieData();

        ll= (LinearLayout)findViewById(R.id.prdlldisplay);
        clickme= (ImageButton) findViewById(R.id.clickme);
        clickme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //attemptLogin();

                //al.add("NewItem"+String.valueOf(i));
                try {

                    clickme.setVisibility(View.INVISIBLE);

                    ll.setVisibility(View.VISIBLE);
                    tv1.setText("Players");
                    tv1.setTextSize(15);

                    params1.setMargins((int)(getApplicationContext().getResources().getDisplayMetrics().density)*5,
                            (int) (getApplicationContext().getResources().getDisplayMetrics().density)*10,
                            (int)    (getApplicationContext().getResources().getDisplayMetrics().density)*0,
                            (int)     (getApplicationContext().getResources().getDisplayMetrics().density)*0);


                    params2.setMargins((int)(getApplicationContext().getResources().getDisplayMetrics().density)*5,
                            (int) (getApplicationContext().getResources().getDisplayMetrics().density)*10,
                            (int)    (getApplicationContext().getResources().getDisplayMetrics().density)*0,
                            (int)     (getApplicationContext().getResources().getDisplayMetrics().density)*0);

                    params3.setMargins((int)(getApplicationContext().getResources().getDisplayMetrics().density)*22,
                            (int) (getApplicationContext().getResources().getDisplayMetrics().density)*10,
                            (int)    (getApplicationContext().getResources().getDisplayMetrics().density)*0,
                            (int)     (getApplicationContext().getResources().getDisplayMetrics().density)*0);
                    tv1.setLayoutParams(params1);
                    tv2.setText("Warming");
                    tv2.setLayoutParams(params2);

                    tv2.setTextSize(15);
                    //params.setMargins(10,10,0,0);
                    tv3.setText("Up");
                    tv3.setLayoutParams(params3);

                    getdatafromdb();
                    /*for (int i = 0; i < 10; i++) {

                        progress=progress+1;

                        handler.postDelayed(new Runnable(){
                            @Override
                            public void run() {

                                mAdapter.notifyDataSetChanged();

                            }
                        },1000 );

                    }
*/
                } catch (Exception e) {
                    e.printStackTrace();
                }




            //update progress


            }
        });


        //**Game Summary Button**//
        Button gameSummary = (Button)findViewById(R.id.simnxtbtn);

        gameSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  String json ="";
               if(json==null){

                   Toast.makeText(MainActivityList.this,"Please Play The Game First",Toast.LENGTH_SHORT).show();
               }
                else{
                Intent intent  = new Intent( MainActivityList.this,GameSummaryActivity.class);
                intent.putExtra("json",json);
                startActivity(intent);
             }
            }
        });

    }

    private void prepareEventMap(String s, HashMap<Integer, String> eventMap) {
try {

    //eventMap = new HashMap<>();
    JSONObject jsonRootObject = new JSONObject(s);

    System.out.println("Start");
    //all Event JSON Object
    JSONObject jsonObjectP1 = jsonRootObject.getJSONObject("Game").getJSONObject("Period1");
    JSONObject jsonObjectP2 = jsonRootObject.getJSONObject("Game").getJSONObject("Period2");
    JSONObject jsonObjectP3 = jsonRootObject.getJSONObject("Game").getJSONObject("Period3");
    JSONArray jsonObjectOT=null;
    if(jsonRootObject.getJSONObject("Game").has("Overtime")){

        jsonObjectOT = jsonRootObject.getJSONObject("Game").getJSONArray("Overtime");
        //setting circle length as 80 min

        pbar.setSecondaryProgress(80); // Secondary Progress
        pbar.setMax(80);

        isOvertime=true;
    }
    ArrayList<Object> JOarr = new ArrayList<>();
    JOarr.add(jsonObjectP1);
    JOarr.add(jsonObjectP2);
    JOarr.add(jsonObjectP3);
if(jsonObjectOT!=null){
        JOarr.add(jsonObjectOT);

    }
    int tmp=0;
    for (Object obj:JOarr ) {


     /*   this.eventMap.put(Integer.getInteger("Period"+tmp),"Period"+tmp);
        */

if(tmp==3)
        {

            System.out.println("In Temp");
            JSONArray arr = (JSONArray)obj;

            Integer occurKey = 0;
            String eventType = "";
            String content = "";
            for (int j = 0; j < arr.length(); j++) {
                if (j != 0) {
                    content = content + "," + (String) arr.get(j);
                } else {
                    occurKey = Integer.parseInt((String) arr.get(0));
                }
            }
            System.out.println(content);

            if(!this.eventMap.containsKey(occurKey)) {
                this.eventMap.put(occurKey, content);
            }
            else{

                this.secondaryEventMap.put(occurKey, content);
            }
        }
else {
    JSONObject jobj=(JSONObject)obj;
    for (int i = 1; i <= jobj.length(); i++) {


        JSONArray arr = jobj.getJSONArray(String.valueOf(i));

        Integer occurKey = 0;
        String eventType = "";
        String content = "";
        for (int j = 0; j < arr.length(); j++) {
            if (j != 0) {
                content = content + "," + (String) arr.get(j);
            } else {
                occurKey = Integer.parseInt((String) arr.get(0));
            }
        }
        System.out.println(content);

        if (!this.eventMap.containsKey(occurKey)) {
            this.eventMap.put(occurKey, content);
        } else {

            this.secondaryEventMap.put(occurKey, content);
        }
    }
}
        tmp++;
    }

}
catch (Exception e){
    e.printStackTrace();
System.out.println("catx");
}
    }

    private class MyCustomAdapter extends BaseAdapter {

        private static final int TYPE_HEADER = 0;
        private static final int TYPE_PP2 = 1;
        private static final int TYPE_GOAL = 2;
        private static final int TYPE_MAX_COUNT = 4;
        private static final int TYPE_GOAL_START =3;
        private String scorer="" ;
        private String assist="";
        private String scoreline="";
        private String scoringteam="";
        private String eventTime="";
        private String goalType="";

        private ArrayList<String> prd1al = new ArrayList();
        private int type=0;
        private String currentText;
        public String colorCode;

        private LayoutInflater mInflater;

        private HashMap<Integer , Integer> typeHM = new HashMap<>();

        public MyCustomAdapter() {
            mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void addPrd1Item(final String text, final String typetext) {
           // mData.add(item);
           // currentText=text;
            switch(typetext){
                case "header":
                    this.type = 0;
                    typeHM.put(prd1al.size(),this.type);
                    prd1al.add(text);
                    break;
                case "PP2":
                    this.type = 1;
                    typeHM.put(prd1al.size(),this.type);
                    prd1al.add(text);
                    //this.colorCode=colorCode;
                    break;
                case "GOAL_START":
                    this.type = 3;
                    typeHM.put(prd1al.size(),this.type);
                    prd1al.add(text);
                    //this.colorCode=colorCode;
                    break;
                case "GOL":
                    this.type = 2;
                    typeHM.put(prd1al.size(),this.type);
                    prd1al.add(text);
                    break;
                default:
                    break;

            }


            //notifyDataSetChanged();
        }


        public void addSeparatorItem(final String item) {
           /* mData.add(item);
            // save separator position
            mSeparatorsSet.add(mData.size() - 1);
            notifyDataSetChanged();*/
        }

        @Override
        public int getItemViewType(int position) {

            //return this.type;
           return typeHM.get(position);
           // return mSeparatorsSet.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
        }

        @Override
        public int getViewTypeCount() {
            return TYPE_MAX_COUNT;
        }

        @Override
        public int getCount() {
            return prd1al.size();
        }

        @Override
        public String getItem(int position) {
            return prd1al.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            int type = getItemViewType(position);

            System.out.println("getView " + position + " " + convertView + " type = " + type);

                switch (type) {
                    case TYPE_HEADER:
                        if (convertView == null) {
                            holder = new ViewHolder();
                            convertView = mInflater.inflate(R.layout.item1, null);
                            holder.publicText = (TextView) convertView.findViewById(R.id.text1);
                            convertView.setTag(holder);
                        }
                        else{
                            holder = (ViewHolder)convertView.getTag();

                        }

                            processEventString(this.prd1al.get(position),"header");
                            holder.publicText.setText( this.currentText);
                        break;
                    case TYPE_PP2:
                        if (convertView == null) {
                            holder = new ViewHolder();
                            convertView = mInflater.inflate(R.layout.item2, null);
                            holder.publicText = (TextView) convertView.findViewById(R.id.text2);
                           // holder.item2ll=(LinearLayout)convertView.findViewById(R.id.item2ll);
                            convertView.setTag(holder);
                        }
                        else{
                            holder = (ViewHolder)convertView.getTag();

                        }

                        processEventString(this.prd1al.get(position),"PP2");


                        holder.publicText.setText(this.currentText);
                        //System.out.println(this.colorCode);
                        //holder.publicText.setBackgroundColor(Color.parseColor(this.colorCode));
                        break;
                    case TYPE_GOAL_START:
                        if (convertView == null) {
                            holder = new ViewHolder();
                            convertView = mInflater.inflate(R.layout.item4, null);
                            holder.publicText = (TextView) convertView.findViewById(R.id.text4);
                            // holder.item2ll=(LinearLayout)convertView.findViewById(R.id.item2ll);
                            convertView.setTag(holder);
                        }
                        else{
                            holder = (ViewHolder)convertView.getTag();

                        }

                        processEventString(this.prd1al.get(position),"GOAL_START");


                        holder.publicText.setText(this.currentText);
                        //System.out.println(this.colorCode);
                        //holder.publicText.setBackgroundColor(Color.parseColor(this.colorCode));
                        break;
                    case TYPE_GOAL:
                        if (convertView == null) {
                            holder = new ViewHolder();
                            convertView = mInflater.inflate(R.layout.item3, null);
                            holder.teamNameText = (TextView) convertView.findViewById(R.id.goaltmnametext);
                            holder.assisttext = (TextView) convertView.findViewById(R.id.assist);
                            holder.scorertext = (TextView) convertView.findViewById(R.id.scorer);
                            holder.timeText = (TextView) convertView.findViewById(R.id.goaltime);


                            convertView.setTag(holder);
                            processEventString(this.prd1al.get(position),"GOL");



                        }
                        else{
                        holder = (ViewHolder)convertView.getTag();
                            processEventString(this.prd1al.get(position),"GOL");

                    }




                        holder.teamNameText.setText(this.scoringteam);
                        if(this.assist!=null)
                        holder.assisttext.setText(this.assist);

                        holder.timeText.setText(this.eventTime);
                        holder.scorertext.setText(this.scorer);
                        break;

                }


            /*else {
                   holder = (ViewHolder)convertView.getTag();


                if(holder.publicText!=null)
                 System.out.println("IN ELSE "+holder.publicText.getText());
            }*/
            //holder.textView.setText(mData.get(position));
            return convertView;
        }

    }

    public static class ViewHolder {
        public TextView teamNameText;
        public TextView timeText;
        public TextView scorertext;
        public  TextView assisttext;
        public TextView publicText;
        public LinearLayout item2ll;
    }

    public class CustomTimerTask extends TimerTask {

        private Handler mHandler = new Handler();

        @Override
        public void run() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {

                            boolean event = false;


                            if (bgngdisplayCount == 0) {
                              //  event = true;
                                //mAdapter.addPrd1Item("--> Players Warming Up", "PP2");

                            } else if (bgngdisplayCount == 2) {
                                //event = true;


                                //mAdapter.addPrd1Item("--> Game Started", "PP2");


                            }

                             if (count == 0) {
                                event = true;


                                ll.setVisibility(View.VISIBLE);
                                 /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT
                                );*/

                                 params1.setMargins((int)(getApplicationContext().getResources().getDisplayMetrics().density)*0,
                                          (int) (getApplicationContext().getResources().getDisplayMetrics().density)*10,
                                          (int)    (getApplicationContext().getResources().getDisplayMetrics().density)*0,
                                          (int)     (getApplicationContext().getResources().getDisplayMetrics().density)*0);
                                tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                                tv1.setText("period");
                                tv1.setLayoutParams(params1);

                                 params2.setMargins((int)(getApplicationContext().getResources().getDisplayMetrics().density)*20,
                                         (int) (getApplicationContext().getResources().getDisplayMetrics().density)*-6,
                                         (int)    (getApplicationContext().getResources().getDisplayMetrics().density)*0,
                                         (int)     (getApplicationContext().getResources().getDisplayMetrics().density)*0);

                                tv2.setTextSize(TypedValue.COMPLEX_UNIT_SP,30);
                                tv2.setText("1");
                                tv2.setLayoutParams(params2);


                                 params3.setMargins((int)(getApplicationContext().getResources().getDisplayMetrics().density)*0,
                                         (int) (getApplicationContext().getResources().getDisplayMetrics().density)*-10,
                                         (int)    (getApplicationContext().getResources().getDisplayMetrics().density)*0,
                                         (int)     (getApplicationContext().getResources().getDisplayMetrics().density)*0);

                                tv3.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                tv3.setText("in progress");
                                tv3.setLayoutParams(params3);
                                mAdapter.addPrd1Item("1st Period", "header");

                            }
                            else if (count == 20) {
                                tv2.setText("2");
                                event = true;
                                mAdapter.addPrd1Item("2nd Period", "header");

                            }
                            else if (count == 40) {
                                tv2.setText("3");

                                event = true;
                                mAdapter.addPrd1Item("3rd Period", "header");

                            }

                            else if(hasEventOccur(count)){
                                event = true;

                                String evntMapString=eventMap.get(Integer.valueOf(count));
                                    processEvent(evntMapString);

                                    if(hasSecondaryEventOccur(count)){
                                        processEvent(secondaryEventMap.get(Integer.valueOf(count)));
                                    }
                            }
                            else if(count==60 && !isOvertime){

                                 event=true;
                                tv1.setText("Earned");
                                tv1.setTextSize(15);
                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT
                                );
                                params.setMargins(0,10,10,10);
                                tv1.setLayoutParams(params);
                                tv2.setText(pointsEarned);
                                tv2.setLayoutParams(params);

                                tv2.setTextSize(20);
                                params.setMargins(20,10,0,0);
                                tv3.setText("Points");
                                tv3.setLayoutParams(params);

                                mAdapter.addPrd1Item("--> Game Over", "PP2");
                                activeSummaryButton=true;
                                cancel();
                            }
                            else if(count==60 && isOvertime)
                            {

                                event=true;
                                tv1.setText("Overtime");
                                tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP,15);
                                tv2.setText("");
                                tv3.setText("In Progress");
                                mAdapter.addPrd1Item("Overtime", "header");
                            }

                            else {
                            //    mAdapter.addPrd1Item("", "text");
                                event=false;
                                System.out.println("No Event");
                            }


                            if(mAdapter.goalType.equalsIgnoreCase("OTG")){
                                event=true;

                                tv1.setText("Earned");
                                tv1.setTextSize(15);
                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT
                                );
                                params.setMargins(0,10,10,10);
                                tv1.setLayoutParams(params);
                                tv2.setText(pointsEarned);
                                tv2.setLayoutParams(params);

                                tv2.setTextSize(20);
                                params.setMargins(10,10,0,0);
                                tv3.setText("Points");
                                tv3.setLayoutParams(params);
                                mAdapter.addPrd1Item("--> Game Over", "PP2");
                                activeSummaryButton=true;
                                cancel();
                            }


                            if(bgngdisplayCount>=2){


                                count++;
                                pbar.setProgress(count);
                            }

                            bgngdisplayCount++;


                            if(event){

                                mAdapter.notifyDataSetChanged();
                                prdonelview.smoothScrollToPosition(mAdapter.getCount()+1);
                            }
                        }
                    });
                }
            }).start();

        }

    }

    private void processEventString(String s,String eventType)
    {
        String[] arr =s.split(",");
        String assist="";

        for(int i=7 ;i<arr.length;i++){
            if (assist!="")
            assist=assist+","+arr[i];
            else
                assist=arr[i];
        }
        mAdapter.assist=assist;
        if(eventType.equalsIgnoreCase("GOL")) {
            mAdapter.eventTime=arr[3];
            mAdapter.scoringteam=arr[2];
            mAdapter.scoreline = arr[5];
            mAdapter.scorer=arr[6];
            mAdapter.goalType=arr[4];



        }

        if(eventType.equalsIgnoreCase("PP2")){

            if(arr.length>1){
                mAdapter.eventTime=arr[3];
            mAdapter.scoringteam=arr[2];
            mAdapter.currentText= "@"+arr[3]+" "+mAdapter.scoringteam+":"+"Power Play";

            }
            else{

                mAdapter.currentText= s;
            }
        }

        if(eventType.equalsIgnoreCase("GOAL_START")){


                mAdapter.currentText= s;
        }

        if(eventType.equalsIgnoreCase("header")){
            mAdapter.currentText= s;
        }
    }

    private void processEvent(String s) {

    //put event in mdapter


        String[] arr =s.split(",");
        String eventType= arr[1];
        String assist="";
        System.out.println("Event Type = "+eventType);
        if(eventType.equalsIgnoreCase("GOL")) {

           // System.out.println(Thread.currentThread().getName());
            mAdapter.addPrd1Item("--> Goal:"+arr[2]+" Scores", "GOAL_START");

            scr1.setText(arr[5].split("-")[0]);
            scr2.setText(arr[5].split("-")[1]);
            mAdapter.addPrd1Item(s,eventType);
        }
        else{
            mAdapter.addPrd1Item(s,eventType);
        }



    }

    private boolean hasEventOccur(int count) {

        if(this.eventMap.containsKey(Integer.valueOf(count)))
        return true;
        else
            return false;
    }
    private boolean hasSecondaryEventOccur(int count) {

        if(this.secondaryEventMap.containsKey(Integer.valueOf(count)))
            return true;
        else
            return false;
    }
    private void getdatafromdb() {

        dbtask = new databaseTaskSimulate(this) ;
        dbtask.execute(guestroster,homeroster);

    }

    public class databaseTaskSimulate extends AsyncTask<String,Void,String> {
        private static final String TAG = "dbactivity";

        Context ctx;

        databaseTaskSimulate(Context ctx) {

            this.ctx = ctx;
        }


        @Override
        protected String doInBackground(String... params) {

            String add_prod_url = gvs.getServerIP()+"/simulate.php";

            String guestTeamRoster = params[0];
            String homeTeamRoster = params[1];

//            if (method.equalsIgnoreCase("simul")) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {

                System.out.println("Check here"+ guestroster+" "+homeroster+" "+userpick+" "+favteam);

                String data = "guestTeamRoster=" + URLEncoder.encode(guestroster, "UTF-8") + "&" +
                        "homeTeamRoster=" + URLEncoder.encode(homeroster, "UTF-8") + "&"
                        + "favteam=" + URLEncoder.encode(favteam, "UTF-8") + "&" +
                        "userpick=" + URLEncoder.encode(userpick, "UTF-8") + "&" +
                        "currgameid=" + URLEncoder.encode(currgameid, "UTF-8") + "&" +
                        "userfantasypick=" + URLEncoder.encode(prepareFantasyPlayerString(), "UTF-8") + "&" +
                        "userid=" + URLEncoder.encode(gvs.getUserid(), "UTF-8");
                ;
                URL url = new URL(add_prod_url);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setFixedLengthStreamingMode(data.getBytes().length);
                http.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
                PrintWriter out = new PrintWriter(http.getOutputStream());
                out.print(data);
                out.close();

                InputStream is = http.getInputStream();
                String output = getStringFromInputStream(is);
                is.close();
                try {

                    return output;

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "error in getting result from server");
                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
//            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            try {

                System.out.println("RESULT = "+result);
                int index= result.indexOf("calculatedscore=");

                String jsonString=result.substring(0,index);
                json=jsonString;
                int indexTotalScore = result.indexOf("totalscore=");
                 pointsEarned=result.substring(index+"calculatedscore=".length(),indexTotalScore);
                String totalScore = result.substring(indexTotalScore+"totalscore=".length());

                prepareEventMap(jsonString,eventMap);
                Timer timer = new Timer();
                TimerTask updateProfile = new CustomTimerTask();
                timer.scheduleAtFixedRate(updateProfile, 1000, 1000);

            }
            catch (Exception e )
            {
                e.printStackTrace();
            }
        }


    }
    private String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

 public String prepareFantasyPlayerString(){

       HashMap<String,ArrayList<String>> hmtmp = gvs.getFantasyPlayersMap();
     StringBuilder sb = new StringBuilder("");
if(hmtmp==null){
   // System.out.println("IN mainActivity map is null");
}
     else{

    System.out.println("Map Size is "+hmtmp.size());

    for (String s:hmtmp.keySet()) {
        System.out.println("Key in MAp "+s);
    }
}
     System.out.println("vt = "+gvs.getVisitingTeam());
    ArrayList<String> alvt = hmtmp.get(gvs.getVisitingTeam());
     ArrayList<String> alht = hmtmp.get(gvs.getHomeTeam());
    System.out.println(alvt.size()+" "+alht.size());

     for (String s:alvt) {
        System.out.println(s);
         sb.append(s.trim());
         sb.append(",");
     }
     for (String s:alht) {
         sb.append(s.trim());
         sb.append(",");
     }


 if (sb.length()>0)
     return sb.toString().substring(0,sb.toString().length()-1);

 return "";
 }

}




