package com.hpp.puckbandit;

import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;


public class NewsFeedFragment extends Fragment {
    GlobalVariableStore gvs;
    HashMap<String , List<String >> dataHashMap;
    NewsFeedAdapter adapter;
    ListView newsFeedListView;
    List<String> data;
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        this.rootView = inflater.inflate(R.layout.activity_news, container, false);

        this.gvs = (GlobalVariableStore) getActivity().getApplicationContext();
        newsFeedListView =(ListView)rootView.findViewById(R.id.newsfeedlistview);

       /* studyStatsView = (ExpandableListView) rootView.findViewById(R.id.news);
        studyTheStats = new NewsDataProvider().getInfo();
        data = new ArrayList<String>(studyTheStats.keySet());
        Collections.sort(data, new Comparator<String>() {

            public int compare(String o1, String o2) {
                int index1 = Integer.parseInt(o1.split(":")[0]);
                int index2 = Integer.parseInt(o2.split(":")[0]);
                return index1 - index2;
            }
        });
        adapter = new NewsAdapter(getActivity(),studyTheStats,data);


        studyStatsView.setAdapter(adapter);
*/

        adapter=new NewsFeedAdapter((MyCustomDrawerLayout) getActivity());
        dataHashMap = new NewsDataProvider(gvs).getInfo();

        for (String key:dataHashMap.keySet()) {
            adapter.addNewsItem(dataHashMap.get(key).get(0),"newsinfo",key);

        }

        adapter.notifyDataSetChanged();

        newsFeedListView.setAdapter(adapter);

        return this.rootView;
    }


}
