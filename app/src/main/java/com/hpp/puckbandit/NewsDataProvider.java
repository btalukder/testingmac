package com.hpp.puckbandit;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by USER on 28-05-2016.
 */
public class NewsDataProvider {


    private GlobalVariableStore gvs;

    public NewsDataProvider(GlobalVariableStore gvs){

        this.gvs=gvs;
    }

    public  HashMap<String, List<String>> getInfo() {
        try {
            HashMap<String, List<String>> details = new HashMap<>();
            List<String> headings = new ArrayList<>();
            List<List <String>> contents = new ArrayList<>();
            List<String> firstInfo = new ArrayList<>();

            NewsData news = new NewsData();
            String jsonNews = news.execute().get();
            int count =1;
            System.out.println(jsonNews);
            JSONObject jsonObject = new JSONObject(jsonNews);
            String heading="";
            String content="";
            Iterator<String> iter = jsonObject.keys();
            int i =0;
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    heading=/*count+":"+*/ key;
                    count++;
                    content=(String)jsonObject.get(key);
                    List<String> info = new ArrayList<>();
                    info.add(content);
                    details.put(heading, info);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            /*firstInfo.add(0, "study the stats of the games care fully");

            List<String> seconfInfo = new ArrayList<>();
            seconfInfo.add("make a pick properly");

            List<String> thirdInfo = new ArrayList<>();
            thirdInfo.add("Run the simulator properly");

            List<String> fourthInfo = new ArrayList<>();
            fourthInfo.add("how To earn Points");


            details.put("Step 1: Study the stats", firstInfo);
            details.put("Step 2: Make a Pick", seconfInfo);
            details.put("Step 3: Run the simulation", thirdInfo);
            details.put("Step 4: How to earn points", fourthInfo);*/

            return details;
        }
            catch(Exception e)
            {
                e.printStackTrace();

            }
        return null;
    }

    public class NewsData extends AsyncTask<String, String, String> {

        NewsData() {

        }

        @Override
        protected String doInBackground(String... params) {

            String userid = "4";
            String add_prod_url = gvs.getServerIP()+"/news.php";

            try {

                String data = "userid=" + URLEncoder.encode(userid, "UTF-8");
                URL url = new URL(add_prod_url);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setFixedLengthStreamingMode(data.getBytes().length);
                http.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
                PrintWriter out = new PrintWriter(http.getOutputStream());
                out.print(data);
                out.close();

                InputStream is = http.getInputStream();
                String output = getStringFromInputStream(is);
                is.close();
                try {

                    return output;

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("dbOperations", "json not decoded");
                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "no data";
        }
        @Override
        protected void onPostExecute(String result) {

        }
        private String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return sb.toString();

        }
    }
}