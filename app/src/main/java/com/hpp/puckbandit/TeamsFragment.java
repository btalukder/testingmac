package com.hpp.puckbandit;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Ankush_PC on 03-06-2016.
 */

public class TeamsFragment extends Fragment {

    String teamNameToShow;
    HashMap<Integer,String> teamPlayerNames= new HashMap<Integer,String>();
    public static final HashMap<String,String> teamNameMap;
    private static GlobalVariableStore gvs;
    static
    {
        teamNameMap =  new HashMap<>();
        teamNameMap.put("CGY" ,"Calgary");
        teamNameMap.put("EDM" ,"Edmonton");
        teamNameMap.put("MTL" ,"Montreal");
        teamNameMap.put("OTT" ,"Ottawa");
        teamNameMap.put("TOR" ,"Toronto");
        teamNameMap.put("VAN" ,"Vancouver");
        teamNameMap.put("WPG","Winnipeg");
    }

    public TeamsFragment() {
    }
    public TeamsFragment(String s) {
        this.teamNameToShow=s;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        gvs=(GlobalVariableStore)getActivity().getApplicationContext();
        View rootView = inflater.inflate(R.layout.team_fragment_layout, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(teamNameToShow);
        TextView tempTeamPlayer ;
        RosterInfo getRosterdata = new RosterInfo();
        try {
            String result = getRosterdata.execute(teamNameToShow).get();
            parseJsonAndPopulate(result);

            for(int i =1;i<=20;i++)
            {
                String viewId="player"+i;

                int id = getResources().getIdentifier(viewId,"id",getActivity().getPackageName());
                tempTeamPlayer = (TextView)rootView.findViewById(id);
                tempTeamPlayer.setText((String)teamPlayerNames.get(i));

            }
            //tempTeamPlayer = (TextView)rootView.findViewById(R.id.Teamname);
            //tempTeamPlayer.setText((String)teamNameMap.get(teamNameToShow));


            DatabaseOperations standing = new DatabaseOperations(getActivity(),gvs);
            Cursor cr = standing.checkLocalDb(standing,teamNameToShow);
            cr.moveToFirst();
            do {

                TextView temp ;
                temp = (TextView) rootView.findViewById(R.id.teamgp);
                temp.setText(cr.getString(1));
                temp = (TextView) rootView.findViewById(R.id.teamw);
                temp.setText(cr.getString(2));
                temp = (TextView) rootView.findViewById(R.id.teaml);
                temp.setText(cr.getString(3));
                temp = (TextView) rootView.findViewById(R.id.tealotl);
                temp.setText(cr.getString(4));
                temp = (TextView) rootView.findViewById(R.id.teamgf);
                temp.setText(cr.getString(5));
                temp = (TextView) rootView.findViewById(R.id.teamga);
                temp.setText(cr.getString(6));
                temp = (TextView) rootView.findViewById(R.id.teamp);
                temp.setText(cr.getString(7));

            }while (cr.moveToNext());
            cr.close();

        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return rootView;
    }


    void parseJsonAndPopulate(String result)
    {
        try {
            JSONObject json = new JSONObject(result);
            JSONObject jsonTeam = json.getJSONObject(teamNameToShow);

            Iterator<String> iter = jsonTeam.keys();
            int i =0;
            String id;
            for (int count=1; count<=20;count++)
            {
                teamPlayerNames.put(count,(String)jsonTeam.get(count+""));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    public class RosterInfo extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... params) {
            String add_prod_url = gvs.getServerIP()+"/getrosterinfo.php";
            String teamname = params[0];


            try {

                String data = "usernameV="+ URLEncoder.encode(teamname,"UTF-8");
                URL url = new URL(add_prod_url);
                HttpURLConnection http= (HttpURLConnection)url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setFixedLengthStreamingMode(data.getBytes().length);
                http.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
                PrintWriter out = new PrintWriter(http.getOutputStream());
                out.print(data);
                out.close();

                InputStream is = http.getInputStream();
                String output = getStringFromInputStream(is);
                is.close();

                return output;

            }catch (MalformedURLException e)
            {
                Log.i("rosterinfo", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.i("rosterinfo", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        private  String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return sb.toString();

        }
    }


}



