package com.hpp.puckbandit;

import android.support.v7.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ankush_PC on 01-06-2016.
 */
public class MyCustomDrawerLayout extends AppCompatActivity {

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ListView mDrawerList2;

    private ArrayAdapter<String> mAdapter2;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    public static final HashMap<String,String> teamNameMap;
    private static GlobalVariableStore gvs;
    static
    {
        teamNameMap =  new HashMap<>();
        teamNameMap.put("calgary","CGY" );
        teamNameMap.put("edmonton","EDM");
        teamNameMap.put("montreal","MTL");
        teamNameMap.put("ottawa","OTT" );
        teamNameMap.put("toronto","TOR");
        teamNameMap.put("vancouver","VAN" );
        teamNameMap.put("winnipeg","WPG");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_main);

        mDrawerList = (ListView)findViewById(R.id.navList);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout1);
        gvs=(GlobalVariableStore)getApplicationContext();
        //mActivityTitle = getTitle().toString();
        mActivityTitle="Scheduled Game";
        getSupportActionBar().setTitle(mActivityTitle);
        mDrawerList2 = (ListView)findViewById(R.id.navList2);

        addDrawerItems();
        addDrawerItemssecond(this);
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeButtonEnabled(true);

        expListView = (ExpandableListView) findViewById(R.id.expdlist);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

    //Child Click Listener

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                /*Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();*/
                mDrawerLayout.closeDrawers();
                loadFragment("teams_"+listDataChild.get(
                        listDataHeader.get(groupPosition)).get(
                        childPosition));
                return false;
            }
        });


    loadHomeFragmentinFirst();

    }

    private void loadHomeFragmentinFirst(){
        Fragment fragment=null;
        mActivityTitle="Scheduled Game";
        //getSupportActionBar().setTitle("Scheduled Game");
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        fragment = new HomeFragment();
        ft.replace(R.id.frame, fragment);
        ft.addToBackStack("HomeFragment");
        ft.commit();

    }

    private void addDrawerItems() {
        final String[] osArray = { "Home", "How To Play", "Standings"};
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               /* Toast.makeText(MyCustomDrawerLayout.this, "Time for an upgrade!", Toast.LENGTH_SHORT).show();*/
                mDrawerLayout.closeDrawers();
                loadFragment(osArray[position]);

            }
        });
    }

    private void addDrawerItemssecond(final Context ctx) {
        final String[] osArray2 = { "Leaderboard", "Reset","NewsFeed","Logout","gamelist"};
        mAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray2);
        mDrawerList2.setAdapter(mAdapter2);

        mDrawerList2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(MyCustomDrawerLayout.this, "Time for an upgrade!", Toast.LENGTH_SHORT).show();

                if(osArray2[position].equalsIgnoreCase("Reset")){

                    resetMethod();
                }
                else if(osArray2[position].equalsIgnoreCase("logout")){

                    //clear all shared preferences and sending to login activity
                    SharedPreferences preferences = getSharedPreferences(LoginActivity.PREFS_NAME, 0);

                    preferences.edit().remove("hasLoggedIn").commit();
                    mDrawerLayout.closeDrawers();

                    Intent intent  = new Intent(ctx ,LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                }
                else if (osArray2[position].equalsIgnoreCase("gamelist")){
                    Fragment fragment=null;
                    mActivityTitle="gamelist";
                    fragment = new GameListView();
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.frame, fragment);
                    ft.addToBackStack("HomeFragment");
                    ft.commit();
                }
                else {
                    mDrawerLayout.closeDrawers();
                    loadFragment(osArray2[position]);
                }
            }
        });
    }


    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings1) {
            return true;
        }

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Teams");

        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("Calgary");
        top250.add("Edmonton");
        top250.add("Montreal");
        top250.add("Ottawa");
        top250.add("Toronto");
        top250.add("Vancouver");
        top250.add("Winnipeg");


        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
    }

public void loadFragment(String fragmentName){

    Fragment fragment=null;



    // Add the fragment to the activity, pushing this transaction
    // on to the back stack.
    FragmentTransaction ft = getFragmentManager().beginTransaction();
    /*ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
            R.animator.fragment_slide_left_exit,
            R.animator.fragment_slide_right_enter,
            R.animator.fragment_slide_right_exit);*/
    if(fragmentName.equalsIgnoreCase("home")){
        //getSupportActionBar().setTitle("Scheduled Game");
        mActivityTitle="Scheduled Game";
                fragment = new HomeFragment();
                ft.replace(R.id.frame, fragment);
                ft.addToBackStack("HomeFragment");
                ft.commit();
    }
    else if(fragmentName.equalsIgnoreCase("standings")){
        fragment = new StandingsFragmentNew();
            mActivityTitle="Standings";
 //       getSupportActionBar().setTitle("Standings");
        ft.replace(R.id.frame, fragment);
        ft.addToBackStack("StandingsFragemt");
        ft.commit();
    }
    else if(fragmentName.equalsIgnoreCase("newsfeed")){
        fragment = new NewsFeedFragment();
        mActivityTitle="News Feed";
        //       getSupportActionBar().setTitle("Standings");
        ft.replace(R.id.frame, fragment);
        ft.addToBackStack("NewsFeedFragemt");
        ft.commit();
    }
    else if(fragmentName.equalsIgnoreCase("how to play")){
        fragment = new HowToPlayFragment();
   //     getSupportActionBar().setTitle("How To Play");
        mActivityTitle="How To Play";
        ft.replace(R.id.frame, fragment);
        ft.addToBackStack("HowToPlayFragemt");
        ft.commit();
    }

    else if(fragmentName.equalsIgnoreCase("leaderboard")){
        fragment = new LeaderBoardFragment();
        //getSupportActionBar().setTitle("Leaderboard");
        mActivityTitle="Leaderboard";
        ft.replace(R.id.frame, fragment);
        ft.addToBackStack("LeaderBoardFragemt");
        ft.commit();
    }

    else if(fragmentName.contains("teams_")){
       // getSupportActionBar().setTitle((fragmentName.split("_")[1])+" Roster");
       mActivityTitle=(fragmentName.split("_")[1])+" Roster";
        fragment = new TeamsFragment(teamNameMap.get((fragmentName.split("_")[1]).toLowerCase()));
        ft.replace(R.id.frame, fragment);
        ft.addToBackStack("TeamFragment");
        ft.commit();
    }

}
    @Override
    public void onBackPressed() {

        if (getFragmentManager().getBackStackEntryCount() > 0) {
           if(getFragmentManager().getBackStackEntryCount()!=1)
            getFragmentManager().popBackStack();


        } else {

            //super.onBackPressed();
        }
    }

    //**Reset Activity**//

    private void resetMethod()
    {
        new AlertDialog.Builder(this)
                .setTitle("Reset")
                .setMessage("Do you really want to reset all your game data? All previous data will be lost")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        resetData();
                    }})
                .setNegativeButton(android.R.string.no, null).show();

    }
    private void resetData()
    {
        DatabaseOperations resetDb = new DatabaseOperations(this,gvs);
        resetDb.resetAll(resetDb,gvs.getUserid());
        Toast.makeText(getBaseContext(), "All Data has been reset.",
                Toast.LENGTH_SHORT).show();
    }

}

