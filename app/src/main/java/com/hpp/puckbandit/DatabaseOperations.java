package com.hpp.puckbandit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by USER on 19-05-2016.
 */
public class DatabaseOperations extends SQLiteOpenHelper {
    Cursor cr;
    DatabaseOperations dbops = this;
    private static GlobalVariableStore gvs;
    public static final int database_version = 1;
    ServerData standing=null;
    ServerData reset=null;
    public DatabaseOperations(Context context,GlobalVariableStore gvs) {
        super(context, TableData.TableInfo.DATABASE_NAME, null, database_version);
        this.gvs=gvs;
        Log.i("database", "Database created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    try{
        String createQuery = "CREATE TABLE "+ TableData.TableInfo.TABLE_NAME+
                "( _id INTEGER PRIMARY KEY AUTOINCREMENT , " +
                TableData.TableInfo.rank+" INTEGER,"+
                 TableData.TableInfo.ref_team_id +" TEXT,"+
                TableData.TableInfo.gp+ " INTEGER," +
                TableData.TableInfo.win +" INTEGER,"+
                TableData.TableInfo.loss +" INTEGER,"+
                TableData.TableInfo.otl +" INTEGER," +
                TableData.TableInfo.gf +" INTEGER," +
                TableData.TableInfo.ga +" INTEGER,"+
                TableData.TableInfo.p +" INTEGER);";

        db.execSQL(createQuery);
        Log.i("database", "Table created");
    }catch (Exception e )
    {
        e.printStackTrace();
    }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void InsertInformation(DatabaseOperations databaseOp,String id,String ref_team_id,
                                  int gp,int win,int loss,int otl,int gf,int ga ,int p,int rank)
    {
        SQLiteDatabase sqlDb = databaseOp.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put(TableData.TableInfo.id , id);
        content.put(TableData.TableInfo.ref_team_id , ref_team_id);
        content.put(TableData.TableInfo.gp , gp);
        content.put(TableData.TableInfo.win , win);
        content.put(TableData.TableInfo.loss , loss);
        content.put(TableData.TableInfo.otl , otl);
        content.put(TableData.TableInfo.gf , gf);
        content.put(TableData.TableInfo.ga ,ga);
        content.put(TableData.TableInfo.p ,p);
        content.put(TableData.TableInfo.rank ,rank);

        long returnVal = sqlDb.insert(TableData.TableInfo.TABLE_NAME,null,content);
        Log.i("database", "data inserted");
    }

    public Cursor checkLocalDb(DatabaseOperations databaseOps,String teamName)
    {
        SQLiteDatabase sqlDb = databaseOps.getReadableDatabase();

        sqlDb.execSQL("delete from " + TableData.TableInfo.TABLE_NAME );

        getDataFromServer();

        String[] columns = {TableData.TableInfo.ref_team_id,
                    TableData.TableInfo.gp,
                    TableData.TableInfo.win,
                    TableData.TableInfo.loss,
                    TableData.TableInfo.otl,
                    TableData.TableInfo.gf ,
                    TableData.TableInfo.ga ,
                    TableData.TableInfo.p ,
                    TableData.TableInfo.rank

        };

            if (teamName.equals("NA")) {
                cr = sqlDb.query(TableData.TableInfo.TABLE_NAME, columns, null, null, null, null, TableData.TableInfo.rank+" ASC");
            }else
            {
                String whereClause = TableData.TableInfo.ref_team_id+"=?";
                String [] whereArgs = {teamName.toString()};
                cr = sqlDb.query(TableData.TableInfo.TABLE_NAME, columns, whereClause, whereArgs, null, null, null);
            }
            return cr;
    }

    private void getDataFromServer() {
        standing = new ServerData();
        try {
            String result=  standing.execute().get();

            String id="";
            String team="";
            int gp=0;
            int win=0;
            int loss=0;
            int otl=0;
            int gf=0;
            int ga=0;
            int p=0;

            JSONObject jsonObject = new JSONObject(result);

            Iterator<String> iter = jsonObject.keys();


            HashMap<String,Integer> ranking = new HashMap<String, Integer>();
            HashMap<String,Float> teamPointsVal = new HashMap<>();
            // to calculate the rank

            float pointsPergame=0;
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    id= key;
                    gp = Integer.parseInt(jsonObject.getJSONArray(id).getString(1));
                    p = Integer.parseInt(jsonObject.getJSONArray(id).getString(7));
                    pointsPergame = (float)p/gp;
                    teamPointsVal.put(id,pointsPergame);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            Set<Entry<String, Float>> set = teamPointsVal.entrySet();
            List<Entry<String, Float>> list = new ArrayList<Entry<String, Float>>(set);
            Collections.sort( list, new Comparator<Map.Entry<String, Float>>()
            {
                public int compare( Map.Entry<String, Float> o1, Map.Entry<String, Float> o2 )
                {
                    return (o2.getValue()).compareTo( o1.getValue() );
                }
            } );

            int rankCheck=1;
            for(Map.Entry<String, Float> entry:list){
                ranking.put(entry.getKey(),rankCheck);
                rankCheck++;
                System.out.println(entry.getKey()+" ==== "+entry.getValue());
            }

            iter = jsonObject.keys();
            int rank=0;
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    id= key;
                    rank =ranking.get(id.toString());
                    team = jsonObject.getJSONArray(id).getString(0);
                    gp = Integer.parseInt(jsonObject.getJSONArray(id).getString(1));
                    win = Integer.parseInt(jsonObject.getJSONArray(id).getString(2));
                    loss = Integer.parseInt(jsonObject.getJSONArray(id).getString(3));
                    otl = Integer.parseInt(jsonObject.getJSONArray(id).getString(4));
                    gf = Integer.parseInt(jsonObject.getJSONArray(id).getString(5));
                    ga = Integer.parseInt(jsonObject.getJSONArray(id).getString(6));
                    p = Integer.parseInt(jsonObject.getJSONArray(id).getString(7));

                    InsertInformation(dbops,id,team,gp,win,loss,otl,gf,ga,p,rank);
                    rank++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public class ServerData extends AsyncTask<String, String, String> {

        ServerData()
        {

        }

        @Override
        protected String doInBackground(String... params) {

            String userid=gvs.getUserid();
            String add_prod_url = gvs.getServerIP()+"/standing.php";
            if (params != null && params.length > 0) {
                if (params[0].equals("resetAll")) {
                    add_prod_url = gvs.getServerIP()+"/reset.php";
                    userid = params[1];
                }
            }
            try {

                String data = "userid=" + URLEncoder.encode(userid, "UTF-8");
                URL url = new URL(add_prod_url);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setFixedLengthStreamingMode(data.getBytes().length);
                http.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
                PrintWriter out = new PrintWriter(http.getOutputStream());
                out.print(data);
                out.close();

                InputStream is = http.getInputStream();
                String output = getStringFromInputStream(is);
                is.close();
                try {

                    return output;

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("dbOperations", "json not decoded");
                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "no data";
        }





        private String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return sb.toString();

        }

    }

    public void resetAll(DatabaseOperations databaseOps,String userid)
    {
        reset = new ServerData();
        reset.execute("resetAll",userid);

    }

}
