package com.hpp.puckbandit;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;

public class RosterView extends AppCompatActivity {
    String teamNameToShow;
    HashMap<Integer,String> teamPlayerNames= new HashMap<Integer,String>();
    public static final HashMap<String,String> teamNameMap;
    static
    {
        teamNameMap =  new HashMap<>();
        teamNameMap.put("CGY" ,"Calgary Flames");
        teamNameMap.put("EDM" ,"Edmonton Oilers");
        teamNameMap.put("MTL" ,"Montrea Canadiens");
        teamNameMap.put("OTT" ,"Ottawa Senators");
        teamNameMap.put("TOR" ,"Toronto Maple Leafs");
        teamNameMap.put("VAN" ,"Vancouver Cancuks");
        teamNameMap.put("WPG","Winnipeg Jets");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        teamNameToShow = bundle.getString("teamname");
        setContentView(R.layout.activity_roster_view);
        TextView tempTeamPlayer ;
        RosterInfo getRosterdata = new RosterInfo();
        try {
            String result = getRosterdata.execute(teamNameToShow).get();
            parseJsonAndPopulate(result);

            for(int i =1;i<=20;i++)
            {
                String viewId="player"+i;

                int id = getResources().getIdentifier(viewId,"id",getPackageName());
                tempTeamPlayer = (TextView)findViewById(id);
                tempTeamPlayer.setText((String)teamPlayerNames.get(i));

            }
            tempTeamPlayer = (TextView)findViewById(R.id.Teamname);
            tempTeamPlayer.setText((String)teamNameMap.get(teamNameToShow));


            DatabaseOperations standing = new DatabaseOperations(this,new GlobalVariableStore());
            Cursor cr = standing.checkLocalDb(standing,teamNameToShow);
            cr.moveToFirst();
            do {

                TextView temp ;
                temp = (TextView) findViewById(R.id.teamgp);
                temp.setText(cr.getString(1));
                temp = (TextView) findViewById(R.id.teamw);
                temp.setText(cr.getString(2));
                temp = (TextView) findViewById(R.id.teaml);
                temp.setText(cr.getString(3));
                temp = (TextView) findViewById(R.id.tealotl);
                temp.setText(cr.getString(4));
                temp = (TextView) findViewById(R.id.teamgf);
                temp.setText(cr.getString(5));
                temp = (TextView) findViewById(R.id.teamga);
                temp.setText(cr.getString(6));
                temp = (TextView) findViewById(R.id.teamp);
                temp.setText(cr.getString(7));

            }while (cr.moveToNext());
            cr.close();

        }catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    void parseJsonAndPopulate(String result)
    {
        try {
            JSONObject json = new JSONObject(result);
            JSONObject jsonTeam = json.getJSONObject(teamNameToShow);

            Iterator<String> iter = jsonTeam.keys();
            int i =0;
            String id;
            for (int count=1; count<=20;count++)
            {
                teamPlayerNames.put(count,(String)jsonTeam.get(count+""));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    public class RosterInfo extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... params) {
            String add_prod_url = "http://10.0.2.2:80/getrosterinfo.php";
            String teamname = params[0];


            try {

                String data = "usernameV="+ URLEncoder.encode(teamname,"UTF-8");
                URL url = new URL(add_prod_url);
                HttpURLConnection http= (HttpURLConnection)url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setFixedLengthStreamingMode(data.getBytes().length);
                http.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
                PrintWriter out = new PrintWriter(http.getOutputStream());
                out.print(data);
                out.close();

                InputStream is = http.getInputStream();
                String output = getStringFromInputStream(is);
                is.close();

               return output;

            }catch (MalformedURLException e)
            {
                Log.i("rosterinfo", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.i("rosterinfo", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        private  String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return sb.toString();

        }
    }


}
