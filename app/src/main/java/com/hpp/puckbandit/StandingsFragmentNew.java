package com.hpp.puckbandit;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Created by Ankush_PC on 03-06-2016.
 */
public class StandingsFragmentNew extends Fragment {


    private Button CGY,EDM,MTL, OTT, TOR,VAN,WPG;
    private Context ctx =getActivity();
    private static GlobalVariableStore gvs;
    public StandingsFragmentNew(){}
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         rootView = inflater.inflate(R.layout.standings_fragment_layout_new, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Standings Fragment");
        gvs=(GlobalVariableStore)getActivity().getApplicationContext();

        TableLayout tl = (TableLayout) rootView.findViewById(R.id.standingcontent);
        TableRow tempTableRow = new TableRow(getActivity());

        TextView textViewnoTeam =  new TextView(getActivity());
        TextView textViewnoGP =  new TextView(getActivity());
        TextView textViewnoW =  new TextView(getActivity());
        TextView textViewnoL =  new TextView(getActivity());
        TextView textViewnoOTL =  new TextView(getActivity());
        TextView textViewnoGF =  new TextView(getActivity());
        TextView textViewnoGA =  new TextView(getActivity());
        TextView textViewnoP =  new TextView(getActivity());

        CGY = new Button(getActivity());
        EDM = new Button(getActivity());
        MTL = new Button(getActivity());
        OTT = new Button(getActivity());
        TOR = new Button(getActivity());
        VAN = new Button(getActivity());
        WPG = new Button(getActivity());
        /////////////////////////////////////////////////

        android.widget.TableLayout.LayoutParams tableParam = new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT);
        //tempTableRow.getLayoutParams().width = TableLayout.LayoutParams.MATCH_PARENT;
        //tempTableRow.getLayoutParams().height = TableLayout.LayoutParams.MATCH_PARENT;
        int padding =(int) rootView.getContext().getResources().getDimension(R.dimen.paddingStandingHead);
        tempTableRow.setPadding(0,0,0,padding);

        tempTableRow.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
        tempTableRow.setBackgroundDrawable(rootView.getContext().getResources().getDrawable(R.drawable.topbottommargin));
        tempTableRow.setLayoutParams(tableParam);

        android.widget.TableRow.LayoutParams trparams = new TableRow.LayoutParams(
                android.widget.TableRow.LayoutParams.WRAP_CONTENT,
                android.widget.TableRow.LayoutParams.WRAP_CONTENT);

        trparams.weight = 1.0f;

        textViewnoTeam.setText("Team");
        textViewnoTeam.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoTeam.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textViewnoTeam.setTextColor(Color.parseColor("#000000"));
        textViewnoTeam.setLayoutParams(trparams);


        textViewnoGP.setText("GP");
        textViewnoGP.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoGP.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textViewnoGP.setTextColor(Color.parseColor("#000000"));
        textViewnoGP.setLayoutParams(trparams);

        textViewnoW.setText("W");
        textViewnoW.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoW.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textViewnoW.setTextColor(Color.parseColor("#000000"));
        textViewnoW.setLayoutParams(trparams);

        textViewnoL.setText("L");
        textViewnoL.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoL.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textViewnoL.setTextColor(Color.parseColor("#000000"));
        textViewnoL.setLayoutParams(trparams);

        textViewnoOTL.setText("OTL");
        textViewnoOTL.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoOTL.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textViewnoOTL.setTextColor(Color.parseColor("#000000"));
        textViewnoOTL.setLayoutParams(trparams);

        textViewnoGF.setText("GF");
        textViewnoGF.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoGF.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textViewnoGF.setTextColor(Color.parseColor("#000000"));
        textViewnoGF.setLayoutParams(trparams);

        textViewnoGA.setText("GA");
        textViewnoGA.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoGA.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textViewnoGA.setTextColor(Color.parseColor("#000000"));
        textViewnoGA.setLayoutParams(trparams);


        textViewnoP.setText("P");
        textViewnoP.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoP.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textViewnoP.setTextColor(Color.parseColor("#000000"));
        textViewnoP.setLayoutParams(trparams);


        tempTableRow.addView(textViewnoTeam);
        tempTableRow.addView(textViewnoGP);
        tempTableRow.addView(textViewnoW);
        tempTableRow.addView(textViewnoL);
        tempTableRow.addView(textViewnoOTL);
        tempTableRow.addView(textViewnoGF);
        tempTableRow.addView(textViewnoGA);
        tempTableRow.addView(textViewnoP);
        tempTableRow.setPadding(0,5,0,10);
        tl.addView(tempTableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

        ////////////////////////////////////////////////

       DatabaseOperations standing = new DatabaseOperations(getActivity(),gvs);
        Cursor cr = standing.checkLocalDb(standing,"NA");
        cr.moveToFirst();
        do {
            String teamName = cr.getString(0);
            TextView temp ;


            tempTableRow=null;
            textViewnoGP =  null;
            textViewnoW =  null;
            textViewnoL =  null;
            textViewnoOTL =  null;
            textViewnoGF =  null;
            textViewnoGA =  null;
            textViewnoP =  null;
            tempTableRow = new TableRow(getActivity());
            tempTableRow.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);


             textViewnoGP =  new TextView(getActivity());
             textViewnoW =  new TextView(getActivity());
             textViewnoL =  new TextView(getActivity());
             textViewnoOTL =  new TextView(getActivity());
             textViewnoGF =  new TextView(getActivity());
             textViewnoGA =  new TextView(getActivity());
             textViewnoP =  new TextView(getActivity());
          //  MTL  = null;
           // MTL = new Button(getActivity());
            switch (teamName) {
                case "MTL":
                    MTL.setText(teamName);
                    MTL.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                    MTL.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    MTL.setLayoutParams(trparams);
                    MTL.setWidth(dpToPx(70));
                    MTL.setTextColor(Color.parseColor("#000000"));
                    tempTableRow.addView(MTL);
                   break;
                case "CGY":
                    CGY.setText(teamName);
                    CGY.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                    CGY.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    CGY.setLayoutParams(trparams);
                    CGY.setWidth(dpToPx(70));
                    CGY.setTextColor(Color.parseColor("#000000"));
                    tempTableRow.addView(CGY);
                    break;
                case "EDM":
                    EDM.setText(teamName);
                    EDM.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                    EDM.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    EDM.setLayoutParams(trparams);
                    EDM.setWidth(dpToPx(70));
                    EDM.setTextColor(Color.parseColor("#000000"));
                    tempTableRow.addView(EDM);
                    break;
                case "OTT":
                    OTT.setText(teamName);
                    OTT.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                    OTT.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    OTT.setLayoutParams(trparams);
                    OTT.setWidth(dpToPx(70));
                    OTT.setTextColor(Color.parseColor("#000000"));
                    tempTableRow.addView(OTT);
                    break;
                case "TOR":
                    TOR.setText(teamName);
                    TOR.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                    TOR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    TOR.setLayoutParams(trparams);
                    TOR.setWidth(dpToPx(70));
                    TOR.setTextColor(Color.parseColor("#000000"));
                    tempTableRow.addView(TOR);
                    break;
                case "VAN":
                    VAN.setText(teamName);
                    VAN.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                    VAN.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    VAN.setLayoutParams(trparams);
                    VAN.setWidth(dpToPx(70));
                    VAN.setTextColor(Color.parseColor("#000000"));
                    tempTableRow.addView(VAN);
                    break;
                case "WPG":
                    WPG.setText(teamName);
                    WPG.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                    WPG.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    WPG.setLayoutParams(trparams);
                    WPG.setWidth(dpToPx(70));
                    WPG.setTextColor(Color.parseColor("#000000"));
                    tempTableRow.addView(WPG);
                    break;
            }


            textViewnoGP.setText(cr.getString(1));
            textViewnoGP.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
            textViewnoGP.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textViewnoGP.setLayoutParams(trparams);
            textViewnoGP.setTextColor(Color.parseColor("#000000"));

            textViewnoW.setText(cr.getString(2));
            textViewnoW.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
            textViewnoW.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textViewnoW.setLayoutParams(trparams);
            textViewnoW.setTextColor(Color.parseColor("#000000"));

            textViewnoL.setText(cr.getString(3));
            textViewnoL.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
            textViewnoL.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textViewnoL.setLayoutParams(trparams);
            textViewnoL.setTextColor(Color.parseColor("#000000"));

            textViewnoOTL.setText(cr.getString(4));
            textViewnoOTL.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
            textViewnoOTL.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textViewnoOTL.setLayoutParams(trparams);
            textViewnoOTL.setTextColor(Color.parseColor("#000000"));

            textViewnoGF.setText(cr.getString(5));
            textViewnoGF.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
            textViewnoGF.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textViewnoGF.setLayoutParams(trparams);
            textViewnoGF.setTextColor(Color.parseColor("#000000"));

            textViewnoGA.setText(cr.getString(6));
            textViewnoGA.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
            textViewnoGA.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textViewnoGA.setLayoutParams(trparams);
            textViewnoGA.setTextColor(Color.parseColor("#000000"));

            textViewnoP.setText(cr.getString(7));
            textViewnoP.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
            textViewnoP.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textViewnoP.setLayoutParams(trparams);
            textViewnoP.setTextColor(Color.parseColor("#000000"));


            tempTableRow.addView(textViewnoGP);
            tempTableRow.addView(textViewnoW);
            tempTableRow.addView(textViewnoL);
            tempTableRow.addView(textViewnoOTL);
            tempTableRow.addView(textViewnoGF);
            tempTableRow.addView(textViewnoGA);
            tempTableRow.addView(textViewnoP);
            tempTableRow.setPadding(0,5,0,5);
            tl.addView(tempTableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

        }while (cr.moveToNext());
        cr.close();

    //    CGY = (Button) rootView.findViewById(R.id.cgy);
      //  EDM = (Button) rootView.findViewById(R.id.edm);

        /*OTT = (Button) rootView.findViewById(R.id.otlb);
        TOR =(Button) rootView.findViewById(R.id.tor);
        VAN =(Button) rootView.findViewById(R.id.van);
        WPG = (Button) rootView.findViewById(R.id.wpg);
*/
        CGY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("CGY");
            }
        });
        EDM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("EDM");

            }
        });
        MTL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // if (MTL.getText().toString().equalsIgnoreCase("MTL"))
                    loadRosterFragment("MTL");
            /*    else if (MTL.getText().toString().equalsIgnoreCase("CGY"))
                    loadRosterFragment("CGY");
                else if (MTL.getText().toString().equalsIgnoreCase("EDM"))
                    loadRosterFragment("EDM");
                else if (MTL.getText().toString().equalsIgnoreCase("OTT"))
                    loadRosterFragment("OTT");
                else if (MTL.getText().toString().equalsIgnoreCase("TOR"))
                    loadRosterFragment("TOR");
                else if (MTL.getText().toString().equalsIgnoreCase("VAN"))
                    loadRosterFragment("VAN");
                else if (MTL.getText().toString().equalsIgnoreCase("WPG"))
                    loadRosterFragment("WPG");*/
            }
        });
        OTT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("OTT");
            }
        });
        TOR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("TOR");
            }
        });
        VAN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("VAN");
            }
        });
        WPG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {loadRosterFragment("WPG");
            }
        });
        return rootView;
    }
    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = rootView.getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
public void loadRosterFragment(String teamname){


    Fragment fragment=null;



    // Add the fragment to the activity, pushing this transaction
    // on to the back stack.
    FragmentTransaction ft = getFragmentManager().beginTransaction();

    fragment = new TeamsFragment(teamname);
    ft.replace(R.id.frame, fragment);
    ft.addToBackStack("TeamsFragment");
    ft.commit();
}

}
