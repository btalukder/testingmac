package com.hpp.puckbandit;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

/**
 * Created by Ankush_PC on 05-06-2016.
 */
public class LeaderBoardFragment extends Fragment {

   // TextView rankView,usernameView,pointsView;
    String usernameGlobal;
    LeaderBoardData leaderBoardData;
    GlobalVariableStore gvs;
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                Bundle savedInstanceState) {

        this.rootView = inflater.inflate(R.layout.leaderboard_fragment, container, false);
        SharedPreferences settings = getActivity().getSharedPreferences(LoginActivity.PREFS_NAME, 0);
        this.gvs = (GlobalVariableStore) getActivity().getApplicationContext();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Schedule Game");
        usernameGlobal=settings.getString("username","ankushb");
        System.out.println("Username from global = "+usernameGlobal);
        leaderBoardData = new LeaderBoardData(getActivity(), usernameGlobal);
        leaderBoardData.execute();

     return rootView;
    }

    public class LeaderBoardData extends AsyncTask<String, Void, String> {

        private final String mUsername;
        private Context ctx;

        LeaderBoardData(Context ctx ,String username) {
            mUsername = username;
            this.ctx= ctx;

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO: attempt authentication against a network service.
            String add_prod_url = gvs.getServerIP()+"/leaderboard.php";


            try {

                String data = "usernameV="+ URLEncoder.encode(mUsername,"UTF-8");
                URL url = new URL(add_prod_url);
                HttpURLConnection http= (HttpURLConnection)url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setFixedLengthStreamingMode(data.getBytes().length);
                http.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
                PrintWriter out = new PrintWriter(http.getOutputStream());
                out.print(data);
                out.close();

                InputStream is = http.getInputStream();
                String output = getStringFromInputStream(is);
                is.close();

                return output;


            }catch (MalformedURLException e)
            {
                Log.i("LeaderBoeard", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.i("LeaderBoeard", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            }

            return "NA";
            // TODO: register the new account here.

        }


        private  String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return sb.toString();

        }

        @Override
        protected void onPostExecute(final String result) {

            try {
                JSONObject jsonObject = new JSONObject(result);
                String id ;
                String rank;
                String username;
                String points;
                String myrank;
                String mypoints;
                Iterator<String> iter = jsonObject.keys();
                TableLayout tl = (TableLayout) rootView.findViewById(R.id.TableLayout01);
                final ScrollView sv = (ScrollView) rootView.findViewById(R.id.ScrollView01);
                TableRow tempTableRow = null;
                TextView tempTextViewRank = null;
                TextView tempTextViewUserName = null;
                TextView tempTextViewPoints = null;
                android.widget.TableRow.LayoutParams trparams = new TableRow.LayoutParams(
                        android.widget.TableRow.LayoutParams.WRAP_CONTENT,
                        android.widget.TableRow.LayoutParams.WRAP_CONTENT);
                trparams.weight = 1.0f;

                int i = 1;
                while (iter.hasNext()) {
                    String key = iter.next();
                    try {
                        rank = key;
                        tempTableRow = null;
                        tempTextViewRank = null;
                        tempTextViewUserName = null;
                        tempTextViewPoints = null;
                        tempTextViewRank = new TextView(getActivity());
                        tempTextViewUserName = new TextView(getActivity());
                        tempTextViewPoints = new TextView(getActivity());
                        tempTableRow = new TableRow(getActivity());

                        tempTextViewRank.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                        tempTextViewRank.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        tempTextViewRank.setLayoutParams(trparams);

                        tempTextViewPoints.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                        tempTextViewPoints.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        tempTextViewPoints.setLayoutParams(trparams);

                        tempTextViewUserName.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                        tempTextViewUserName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        tempTextViewUserName.setLayoutParams(trparams);


                        tempTextViewUserName.setTextColor(Color.parseColor("#000000"));
                        tempTextViewPoints.setTextColor(Color.parseColor("#000000"));
                        tempTextViewRank.setTextColor(Color.parseColor("#000000"));


                        tempTableRow.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);

                        if (rank.equals("hpp"))
                        {
                            /*rankView =(TextView) rootView.findViewById(R.id.myrank) ;
                            usernameView =(TextView)rootView. findViewById(R.id.myusername) ;
                            pointsView =(TextView) rootView.findViewById(R.id.mypoints) ;*/
                            myrank = jsonObject.getJSONArray(rank).getString(0);
                            //rankView.setText(myrank);
                            points = jsonObject.getJSONArray(rank).getString(1);
                            //pointsView.setText(points);
                            //usernameView.setText(usernameGlobal);
                            tempTableRow.setBackgroundColor(Color.parseColor("#c2c2a3"));
                            tempTextViewRank.setText((String) myrank);
                            tempTextViewUserName.setText((String) usernameGlobal);
                            tempTextViewPoints.setText((String) points);
                        }
                        else {
                            username = jsonObject.getJSONArray(rank).getString(0);
                            points = jsonObject.getJSONArray(rank).getString(1);

                           /* String rankViewId = "rank" + i;
                            String nameViewId = "name" + i;
                            String pointsViewId = "points" + i;*/



                            tempTextViewRank.setText((String) rank);
                            tempTextViewUserName.setText((String) username);
                            tempTextViewPoints.setText((String) points);


                            /*int idVal = getResources().getIdentifier(rankViewId, "id", getActivity().getPackageName());
                            rankView = (TextView)rootView.findViewById(idVal);
                            rankView.setText((String) rank);

                            idVal = getResources().getIdentifier(nameViewId, "id",getActivity().getPackageName());
                            usernameView = (TextView)rootView.findViewById(idVal);
                            usernameView.setText((String) username);

                            idVal = getResources().getIdentifier(pointsViewId, "id",getActivity().getPackageName());
                            pointsView = (TextView)rootView.findViewById(idVal);
                            pointsView.setText((String) points);*/
                            i++;

                        }
                        tempTableRow.addView(tempTextViewRank);
                        tempTableRow.addView(tempTextViewUserName);
                        tempTableRow.addView(tempTextViewPoints);
                        tempTableRow.setPadding(0,5,0,5);
                        tl.addView(tempTableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {

        }
    }

}
