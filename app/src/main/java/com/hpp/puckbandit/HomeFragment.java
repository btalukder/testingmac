package com.hpp.puckbandit;

/**
 * Created by Ankush_PC on 01-06-2016.
 */

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment {


    String username;
    HashMap<String,List<String >> menus;
    List<String> menuItems;
    ExpandableListView exp_list;
    TeamsAdapter adapter;
    TextView score,scheduleTeam1,scheduleTeam2,scoreVal;
    RadioButton road,home;
    Switch switch1;
    View rootView;
    Button submit;
    RadioGroup rgrp;
    GlobalVariableStore globalVariable;
    public HomeFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         this.rootView = inflater.inflate(R.layout.home_fragment_layout, container, false);
        username ="ankushb";

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Schedule Game");
        scheduleTeam1 = (TextView) rootView.findViewById(R.id.scheduleTeam_HF);
        scheduleTeam2 = (TextView)rootView.findViewById(R.id.scheduleTeam2_HF);
        this.globalVariable = (GlobalVariableStore) getActivity().getApplicationContext();

        road = (RadioButton) rootView.findViewById(R.id.road_HF);
        home = (RadioButton) rootView.findViewById(R.id.home_HF);
        rgrp=(RadioGroup) rootView.findViewById(R.id.toggle);

        rgrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {

            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // TODO Auto-generated method stub
                if(road.isChecked())
                {
                    TextView tv = (TextView)rootView.findViewById(R.id.pickedtext);
                    tv.setText("You Have Picked "+String.valueOf( road.getText()).split(" ")[0]);

                }
                else if(home.isChecked())
                {

                    TextView tv = (TextView)rootView.findViewById(R.id.pickedtext);
                    tv.setText("You Have Picked "+String.valueOf( home.getText()).split(" ")[0]);

                }
            }
        });
        GetSchedule operation = new GetSchedule(getActivity());
        try {
            String result = operation.execute(username).get();
            parseData(result);

            submit=(Button)rootView.findViewById(R.id.submit_HF);

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //go to simulation page

                    int selectedId=rgrp.getCheckedRadioButtonId();
                    RadioButton rb=(RadioButton)rootView.findViewById(selectedId);


                    if(!road.isChecked()&& !home.isChecked()){
                        Toast.makeText(getActivity(), "Please Pick Either Of The Team!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        System.out.println("My PICK =" + String.valueOf(rb.getText()));
                        globalVariable.setMyPick(String.valueOf(rb.getText()).split(" ")[0].toLowerCase());

                        Fragment fragment=null;
                        FragmentTransaction ft = getFragmentManager().beginTransaction();

                        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Select Fantasy Players("+globalVariable.getVisitingTeam()+")");
                        fragment = new FantasyPlayerSelectFragment(globalVariable.getVisitingTeam());
                        ft.replace(R.id.frame, fragment);
                        ft.addToBackStack("FantasyPlayerSelectFragment");
                        ft.commit();
                        /*Intent intent = new Intent(getActivity(), MainActivityList.class);
                        startActivity(intent);*/
                    }
                }
            });

        }catch (Exception e)
        {
            e.printStackTrace();
        }






        return rootView;
    }

    void parseData(String data)
    {
        try {
            TextView tempVal ;
            String team1Name,team2Name;
            DatabaseOperations standing = new DatabaseOperations(getActivity(),globalVariable);
            JSONArray array = new JSONArray(data);
            globalVariable.setCurrentGameId(array.getString(2));
            String temp = array.getString(0);
            String teamNameToShow = temp;
            team1Name = teamNameToShow;
            tempVal = (TextView)rootView.findViewById(R.id.team2);
            tempVal.setText(teamNameToShow);


            String teamName = TeamsFragment.teamNameMap.get(temp);

            scheduleTeam1.setText(teamName);
            System.out.println("V Team "+temp);
            globalVariable.setVisitingTeam(temp);

            road.setText(teamName);


            Cursor cr = standing.checkLocalDb(standing,teamNameToShow);
            cr.moveToFirst();
            int gp1 =0;
            int points1 = 0;
            do {


                tempVal = (TextView) rootView.findViewById(R.id.team2gp);
                tempVal.setText(cr.getString(1));
                gp1 = Integer.parseInt(cr.getString(1));
                tempVal = (TextView) rootView.findViewById(R.id.team2w);
                tempVal.setText(cr.getString(2));
                tempVal = (TextView)rootView. findViewById(R.id.team2l);
                tempVal.setText(cr.getString(3));
                tempVal = (TextView) rootView.findViewById(R.id.team2otl);
                tempVal.setText(cr.getString(4));
                tempVal = (TextView) rootView.findViewById(R.id.team2gf);
                tempVal.setText(cr.getString(5));
                tempVal = (TextView) rootView.findViewById(R.id.team2ga);
                tempVal.setText(cr.getString(6));
                tempVal = (TextView) rootView.findViewById(R.id.team2p);
                tempVal.setText(cr.getString(7));
                points1 = Integer.parseInt(cr.getString(7));

            }while (cr.moveToNext());
            cr.close();

            temp = array.getString(1);
            System.out.println("Home Team "+temp);
            globalVariable.setHomeTeam(temp);

            teamName = TeamsFragment.teamNameMap.get(temp);
            scheduleTeam2.setText(teamName);
            home.setText(teamName);

            teamNameToShow = temp;
            team2Name = teamNameToShow;
            tempVal = (TextView)rootView.findViewById(R.id.Team1);
            tempVal.setText(teamNameToShow);
            cr = standing.checkLocalDb(standing,teamNameToShow);
            cr.moveToFirst();
            int gp2 = 0;
            int point2=0;
            do {


                tempVal = (TextView) rootView.findViewById(R.id.team1gp);
                tempVal.setText(cr.getString(1));
                gp2 = Integer.parseInt(cr.getString(1));
                tempVal = (TextView) rootView.findViewById(R.id.team1w);
                tempVal.setText(cr.getString(2));
                tempVal = (TextView) rootView.findViewById(R.id.team1l);
                tempVal.setText(cr.getString(3));
                tempVal = (TextView) rootView.findViewById(R.id.team1otl);
                tempVal.setText(cr.getString(4));
                tempVal = (TextView)rootView.findViewById(R.id.team1gf);
                tempVal.setText(cr.getString(5));
                tempVal = (TextView) rootView.findViewById(R.id.team1ga);
                tempVal.setText(cr.getString(6));
                tempVal = (TextView) rootView.findViewById(R.id.team1p);
                tempVal.setText(cr.getString(7));
                point2 = Integer.parseInt(cr.getString(7));

            }while (cr.moveToNext());
            cr.close();

            String favTeam="";
            float team1fav = (float)points1/(float)gp1;
            float team2fav = (float)point2/(float)gp2;

            System.out.println(team1fav+" "+team2fav);
            if (team1fav >team2fav )
            {
                favTeam = team1Name;
            }
            else
            {
                favTeam =  team2Name;
            }
            if (favTeam.equals(team1Name))
            {
                road.setText(road.getText()+" "+"\n"+"+100/-150");
                home.setText(home.getText()+" "+"\n"+"+150/-100");
            }
            else
            {
                road.setText(road.getText()+" "+"\n"+"+150/-100");
                home.setText(home.getText()+" "+"\n"+"+100/-150");
            }

                globalVariable.setFavrtteam(favTeam);


        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public class GetSchedule extends AsyncTask<String,Void,String> {

        private Context ctx;
        public GetSchedule (Context ctx)
        {
            this.ctx = ctx;
        }
        @Override
        protected String doInBackground(String... params) {
            String add_prod_url = globalVariable.getServerIP()+"/getalldata.php";

            String username = params[0];
            try {

                System.out.println("userid= "+globalVariable.getUserid());
                String data = "username=" + URLEncoder.encode(username, "UTF-8")+ "&" +
                        "userid=" + URLEncoder.encode(globalVariable.getUserid(), "UTF-8") ;
                URL url = new URL(add_prod_url);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setFixedLengthStreamingMode(data.getBytes().length);
                http.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
                PrintWriter out = new PrintWriter(http.getOutputStream());
                out.print(data);
                out.close();

                InputStream is = http.getInputStream();
                String output = getStringFromInputStream(is);
                is.close();
                try {

                    return output;

                } catch (Exception e) {
                    e.printStackTrace();

                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        private String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return sb.toString();

        }
    }



}