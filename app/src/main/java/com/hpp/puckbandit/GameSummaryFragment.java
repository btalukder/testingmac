package com.hpp.puckbandit;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by USER on 09-06-2016.
 */
public class GameSummaryFragment extends Fragment {
    /** Called when the activity is first created. */
    Button btn;
    int counter=0;

    TextView roadTeamHead,roadTeamHeadGoal;
    TextView homeTeamHead,homeTeamHeadGoal;
    View rootView;
    private GlobalVariableStore gvs;
    String json;
    public  GameSummaryFragment(String jsonString){
        this.json = jsonString;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.rootView = inflater.inflate(R.layout.game_summary_layout, container, false);
        //String json ="{\"Game\":{\"USERNO\":\"1\",\"NO\":\"1\",\"Final\":{\"Edmonton\":{\"Goals\":\"5\",\"FanPlayers\":{\"C. McDavid\":{\"G\":\"0\",\"A\":\"1\"},\"M. Hendricks\":{\"G\":\"0\",\"A\":\"1\"},\"L. Korpikoski\":{\"G\":\"1\",\"A\":\"0\"},\"M. Letestu\":{\"G\":\"2\",\"A\":\"1\"},\"P. Maroon\":{\"G\":\"1\",\"A\":\"1\"},\"J. Eberie\":{\"G\":\"0\",\"A\":\"1\"},\"Z. Kassin\":{\"G\":\"0\",\"A\":\"1\"},\"T. Hall\":{\"G\":\"1\",\"A\":\"0\"}}},\"Ottawa\":{\"Goals\":\"3\",\"FanPlayers\":{\"N. Paul\":{\"G\":\"1\",\"A\":\"0\"},\"Z. Smith\":{\"G\":\"0\",\"A\":\"1\"},\"M. Puempel\":{\"G\":\"1\",\"A\":\"0\"},\"R. Dzingel\":{\"G\":\"0\",\"A\":\"1\"},\"B. Robinson\":{\"G\":\"1\",\"A\":\"0\"},\"M. Kostka\":{\"G\":\"0\",\"A\":\"2\"}}}},\"FanTasyStrength\":\"238\",\"Period1\":{\"1\":[\"4\",\"GOL\",\"Edmonton\",\"4:20\",\"5-on-5\",\"1-0\",\"T. Hall\",\"Z. Kassin\"],\"2\":[\"6\",\"PP2\",\"Ottawa\",\"6:00\"],\"3\":[\"14\",\"PP2\",\"Edmonton\",\"14:00\"],\"4\":[\"15\",\"GOL\",\"Edmonton\",\"15:53\",\"PPG\",\"2-0\",\"L. Korpikoski\",\"C. McDavid\",\"M. Hendricks\"],\"5\":[\"16\",\"PP2\",\"Edmonton\",\"16:02\"],\"6\":[\"18\",\"PP2\",\"Edmonton\",\"18:05\"]},\"Period2\":{\"1\":[\"22\",\"PP2\",\"Edmonton\",\"2:06\"],\"2\":[\"24\",\"GOL\",\"Edmonton\",\"4:28\",\"5-on-5\",\"3-0\",\"M. Letestu\",\"P. Maroon\"],\"3\":[\"24\",\"PP2\",\"Edmonton\",\"4:54\"],\"4\":[\"27\",\"PP2\",\"Edmonton\",\"7:00\"],\"5\":[\"30\",\"GOL\",\"Edmonton\",\"10:42\",\"5-on-5\",\"4-0\",\"P. Maroon\",\"M. Letestu\"],\"6\":[\"32\",\"PP2\",\"Ottawa\",\"12:00\"]},\"Period3\":{\"1\":[\"43\",\"GOL\",\"Ottawa\",\"3:41\",\"5-on-5\",\"4-1\",\"B. Robinson\",\"R. Dzingel\"],\"2\":[\"49\",\"PP2\",\"Ottawa\",\"9:02\"],\"3\":[\"51\",\"GOL\",\"Ottawa\",\"11:17\",\"5-on-5\",\"4-2\",\"N. Paul\",\"Z. Smith\",\"M. Kostka\"],\"4\":[\"54\",\"GOL\",\"Ottawa\",\"14:02\",\"5-on-5\",\"4-3\",\"M. Puempel\",\"M. Kostka\"],\"5\":[\"58\",\"ENT\",\"Ottawa\"],\"6\":[\"59\",\"GOL\",\"Edmonton\",\"19:31\",\" EN\",\"5-3\",\"M. Letestu\",\"J. Eberie\"]}}}";

        gvs=(GlobalVariableStore)getActivity().getApplicationContext();
        roadTeamHead = (TextView) rootView.findViewById(R.id.roadTeamNameHead);
        roadTeamHeadGoal = (TextView)rootView.findViewById(R.id.roadTeamHeadGoal);
        homeTeamHead = (TextView)rootView.findViewById(R.id.homeTeamGoalHead);
        homeTeamHeadGoal = (TextView)rootView.findViewById(R.id.homeTeamHeadGoal);
        init(json);

        Button nextGame=(Button)rootView.findViewById(R.id.nextBtn_GS);

        nextGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent  = new Intent(getActivity(),MyCustomDrawerLayout.class);
                startActivity(intent);
            }
        });

        return rootView;
    }

    public void init(String json){
        try {

            TableLayout tl = (TableLayout) rootView.findViewById(R.id.TableLayout01);
//            TableLayout t2 = (TableLayout) findViewById(R.id.TableLayout02);
            TableRow tr = new TableRow(getActivity());
            TableRow tr1 = new TableRow(getActivity());
            tr1.setBackgroundColor(Color.parseColor("#3366ff"));
            List<TableRow> trList = new ArrayList<TableRow>();
            List<TableData> tdList = new ArrayList<TableData>();
            TextView tv0 = new TextView(getActivity());


            tv0.setTextColor(Color.BLACK);
            tv0.setGravity(Gravity.LEFT);
            tv0.setWidth(120);
            TextView tv1 = new TextView(getActivity());

            tv1.setText("G");
            tv1.setTextColor(Color.BLACK);
            tv1.setGravity(Gravity.CENTER);
            tv1.setPadding(10,10,10,10);
            TextView tv2 = new TextView(getActivity());
            tv2.setText("A");
            tv2.setTextColor(Color.BLACK);
            tv2.setGravity(Gravity.CENTER);
            tv2.setPadding(10,10,10,10);


            TextView tv3 = new TextView(getActivity());


            tv3.setTextColor(Color.BLACK);
            tv3.setGravity(Gravity.LEFT);
            tv3.setWidth(120);
            TextView tv4 = new TextView(getActivity());

            tv4.setText("G");
            tv4.setTextColor(Color.BLACK);
            tv4.setGravity(Gravity.CENTER);
            tv4.setPadding(10,10,10,10);

            TextView tv5 = new TextView(getActivity());
            tv5.setText("A");
            tv5.setTextColor(Color.BLACK);
            tv5.setGravity(Gravity.CENTER);
            tv5.setPadding(10,10,10,10);


            JSONObject root = new JSONObject(json);
            JSONObject game = root.getJSONObject("Game");
            JSONObject final1 = game.getJSONObject("Final");

            Iterator<String> iter = final1.keys();
            int i =0;
            String roadTeamGoal = "";
            String homeTeamGoal="";
            String roadTeamName ="";
            String homeTeamName ="";
            TableRow tempTableRow = null;
            TextView tempTextViewName = null;
            TextView tempTextViewGoal = null;
            TextView tempTextViewAssist = null;
            ArrayList<String> fantasyPlayers;
            while (iter.hasNext()) {
                String key = iter.next();
                if (i == 0)
                {
                    i++;
                    roadTeamName = key;

                    roadTeamGoal = (String)final1.getJSONObject(key).get("Goals");
//                    String roadHead = roadTeamName+"\t" + roadTeamGoal;
                    fantasyPlayers=gvs.getFantasyPlayersMap().get(roadTeamName.toUpperCase());

                    roadTeamHead.setText(roadTeamName);
                    roadTeamHeadGoal.setText(roadTeamGoal);
                    JSONObject fanPlayers = final1.getJSONObject(key).getJSONObject("FanPlayers");
                    tv0.setText(gvs.threetoFullNameMap.get(roadTeamName.toUpperCase()));
                    tr.addView(tv0);
                    tr.addView(tv1);
                    tr.addView(tv2);
                    tr.setBackgroundColor(Color.parseColor("#3366ff"));

                    tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    Iterator<String> players = fanPlayers.keys();
                    while (players.hasNext()) {
                        tempTableRow=null;
                        tempTextViewName = null;
                        tempTextViewGoal =null;
                        tempTextViewAssist=null;

                        tempTextViewName= new TextView(getActivity());
                        tempTextViewAssist= new TextView(getActivity());
                        tempTextViewGoal= new TextView(getActivity());
                        tempTableRow = new TableRow(getActivity());
                        String playerName = players.next();
                        tempTextViewName.setText(playerName);
                        tempTextViewName.setPadding(10,10,10,10);

                        String goal = (String)fanPlayers.getJSONObject(playerName).get("G");
                        tempTextViewGoal.setText(goal);
                        tempTextViewGoal.setPadding(10,10,10,10);
                        String assist = (String)fanPlayers.getJSONObject(playerName).get("A");
                        tempTextViewAssist.setText(assist);
                        tempTextViewAssist.setPadding(10,10,10,10);
                        tempTableRow.addView(tempTextViewName);
                        tempTableRow.addView(tempTextViewGoal);
                        tempTableRow.addView(tempTextViewAssist);
                        if(fantasyPlayers.contains(playerName)){

                            tempTableRow.setBackgroundColor(Color.parseColor("#c2c2a3"));
                        }

                        tl.addView(tempTableRow,new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    }


                    //tr.addView(fan);
                    //tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

                }
                else
                {
                    homeTeamName = key;
                    homeTeamGoal = (String)final1.getJSONObject(key).get("Goals");
                    String homeHead = homeTeamGoal+"  " + homeTeamName;
                    fantasyPlayers=gvs.getFantasyPlayersMap().get(homeTeamName.toUpperCase());

                    homeTeamHead.setText(homeTeamName);
                    homeTeamHeadGoal.setText(homeTeamGoal);
                    JSONObject fanPlayers = final1.getJSONObject(key).getJSONObject("FanPlayers");
                    tv3.setText(gvs.threetoFullNameMap.get(homeTeamName.toUpperCase()));
                    tr1.addView(tv3);
                    tr1.addView(tv4);
                    tr1.addView(tv5);
                    tl.addView(tr1, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

                    Iterator<String> players = fanPlayers.keys();
                    while (players.hasNext()) {
                        tempTableRow=null;
                        tempTextViewName = null;
                        tempTextViewGoal =null;
                        tempTextViewAssist=null;

                        tempTextViewName= new TextView(getActivity());
                        tempTextViewAssist= new TextView(getActivity());
                        tempTextViewGoal= new TextView(getActivity());
                        tempTableRow = new TableRow(getActivity());
                        String playerName = players.next();
                        tempTextViewName.setText(playerName);
                        tempTextViewName.setPadding(10,10,10,10);

                        String goal = (String)fanPlayers.getJSONObject(playerName).get("G");
                        tempTextViewGoal.setText(goal);
                        tempTextViewGoal.setPadding(10,10,10,10);
                        String assist = (String)fanPlayers.getJSONObject(playerName).get("A");
                        tempTextViewAssist.setText(assist);
                        tempTextViewAssist.setPadding(10,10,10,10);
                        tempTableRow.addView(tempTextViewName);
                        tempTableRow.addView(tempTextViewGoal);
                        tempTableRow.addView(tempTextViewAssist);
                        if(fantasyPlayers.contains(playerName)){

                            tempTableRow.setBackgroundColor(Color.parseColor("#c2c2a3"));
                        }
                        tl.addView(tempTableRow,new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    }
                }
            }



            //tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
            //tl.addView(tr1, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

        }catch(Exception e)
        {
            System.out.print(e.getMessage());
            e.printStackTrace();
        }
    }

}
