package com.hpp.puckbandit;

import android.app.Fragment;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

import com.hpp.puckbandit.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by USER on 09-06-2016.
 */
public class GameListView extends Fragment {
    /** Called when the activity is first created. */
    Button btn;
    int counter=0;


    View rootView;
    private GlobalVariableStore gvs;
    String json;
    public  GameListView(){


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.gamelist_view, container, false);
        try {

            gvs = (GlobalVariableStore) getActivity().getApplicationContext();
            String json = new GameInfo().execute().get();
            btn = (Button)rootView.findViewById(R.id.nextBtn_list);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (btn.getText().toString().trim().equals("All games played . Please reset")) {
                        DatabaseOperations resetDb = new DatabaseOperations(rootView.getContext(), gvs);
                        resetDb.resetAll(resetDb, gvs.getUserid());
                        Toast.makeText(rootView.getContext(), "All Data has been reset.",
                                Toast.LENGTH_SHORT).show();
                    }
                    else
                    // TO DO need to place the proper intent
                    Toast.makeText(rootView.getContext(), "need to place proper intent",
                            Toast.LENGTH_SHORT).show();
                    /*Intent intent  = new Intent(getActivity(),MyCustomDrawerLayout.class);
                    startActivity(intent);*/
                }
            });

            init(json);

            return rootView;
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return rootView;
    }

    public void init(String json){
        try {

            TableLayout tl = (TableLayout) rootView.findViewById(R.id.TableLayout01);
            final ScrollView sv = (ScrollView) rootView.findViewById(R.id.ScrollView01);
//            TableLayout t2 = (TableLayout) findViewById(R.id.TableLayout02);
            /*TableRow tr = new TableRow(getActivity());
            TableRow tr1 = new TableRow(getActivity());
            tr1.setBackgroundColor(Color.parseColor("#3366ff"));
            List<TableRow> trList = new ArrayList<TableRow>();
            List<TableData> tdList = new ArrayList<TableData>();
            TextView tv0 = new TextView(getActivity());


            tv0.setTextColor(Color.BLACK);
            tv0.setGravity(Gravity.CENTER);

            tv0.setText("Game#");
            TextView tv1 = new TextView(getActivity());

            tv1.setText("Road Team");
            tv1.setTextColor(Color.BLACK);
            tv1.setGravity(Gravity.CENTER);
            tv1.setPadding(10,10,10,10);
            TextView tv2 = new TextView(getActivity());
            tv2.setText("Home Team");
            tv2.setTextColor(Color.BLACK);
            tv2.setGravity(Gravity.CENTER);
            tv2.setPadding(10,10,10,10);


            tr.addView(tv0);
            tr.addView(tv1);
            tr.addView(tv2);
            tr.setBackgroundColor(Color.parseColor("#3366ff"));
            tr.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);

            tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));*/

            TableRow tempTableRow = null;
            TextView tempTextViewno = null;
            TextView tempTextViewRoadTeam = null;
            TextView tempTextViewHomeTeam = null;

            JSONObject jsonObject = new JSONObject(json);


            String HomeTeam="";
            String lastGamePlayed=jsonObject.getString("last_game");
            if (lastGamePlayed.equals("127"))
                btn.setText("  All games played . Please reset ");
            else {
                int tempVar= Integer.parseInt(lastGamePlayed)+126;
                btn.setText("  Play Game # " + tempVar + "  ");
            }
            Iterator<String> iter = jsonObject.keys();
            int i =0;


            while (iter.hasNext()) {
                String key = iter.next();
                try {


                   String gameNo= key.trim();
                    String showGameno=gameNo;
                    if(key.equalsIgnoreCase("last_game"))
                    {
                        continue;
                    }
                        int changeGameStart = Integer.parseInt(gameNo)+126;
                        showGameno =""+changeGameStart;
                        String roadTeam = jsonObject.getJSONArray(key).getString(0);
                        String homeTeam = jsonObject.getJSONArray(key).getString(1);


                        tempTableRow = null;
                        tempTextViewno = null;
                        tempTextViewRoadTeam = null;
                        tempTextViewHomeTeam = null;


                        tempTextViewno = new TextView(getActivity());
                        tempTextViewRoadTeam = new TextView(getActivity());
                        tempTextViewHomeTeam = new TextView(getActivity());
                        tempTableRow = new TableRow(getActivity());
                        tempTableRow.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
                        tempTextViewno.setText(showGameno);


                        roadTeam = TeamsFragment.teamNameMap.get(roadTeam);
                        tempTextViewRoadTeam.setText(roadTeam);


                        homeTeam = TeamsFragment.teamNameMap.get(homeTeam);
                        tempTextViewHomeTeam.setText(homeTeam);


                    tempTextViewno.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                    tempTextViewno.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);



                    android.widget.TableRow.LayoutParams trparams = new TableRow.LayoutParams(
                            android.widget.TableRow.LayoutParams.WRAP_CONTENT,
                            android.widget.TableRow.LayoutParams.WRAP_CONTENT);

//                    params1 =  new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
                    trparams.setMargins(0, 0,
                            (int)(rootView.getContext().getResources().getDisplayMetrics().density)*1, 0);
                    trparams.weight = 1.0f;
                    tempTextViewno.setLayoutParams(trparams);
                   tempTextViewno.setTextColor(Color.parseColor("#000000"));
                    tempTextViewRoadTeam.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                    tempTextViewRoadTeam.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//                    tempTextViewRoadTeam.setPadding(10,0,0,0);
                    tempTextViewRoadTeam.setLayoutParams(trparams);
//
                    tempTextViewHomeTeam.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
                    tempTextViewHomeTeam.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    tempTextViewHomeTeam.setLayoutParams(trparams);
                    tempTextViewRoadTeam.setTextColor(Color.parseColor("#000000"));
                    tempTextViewHomeTeam.setTextColor(Color.parseColor("#000000"));


                        if (lastGamePlayed.equalsIgnoreCase(gameNo)) {

                            tempTableRow.setBackgroundColor(Color.parseColor("#c2c2a3"));



                        }
                        else if (Integer.parseInt(lastGamePlayed) > Integer.parseInt(gameNo))
                        {
                            tempTextViewno.setTextColor(Color.parseColor("#15c132"));
                            tempTextViewRoadTeam.setTextColor(Color.parseColor("#15c132"));
                            tempTextViewHomeTeam.setTextColor(Color.parseColor("#15c132"));
                            //tempTableRow.setBackgroundColor(Color.parseColor("#15c132"));
                        }
                    tempTableRow.addView(tempTextViewno);
                    tempTableRow.addView(tempTextViewRoadTeam);
                    tempTableRow.addView(tempTextViewHomeTeam);
                    tempTableRow.setPadding(0,5,0,5);
                        tl.addView(tempTableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            int last=Integer.parseInt(lastGamePlayed);
            if ( last> 5)
                last = last -4;

            if (last >= 4) {
                final View child = tl.getChildAt(last);
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        sv.smoothScrollTo(0, child.getBottom());
                    }
                });
            }
            //tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
            //tl.addView(tr1, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

        }catch(Exception e)
        {
            System.out.print(e.getMessage());
            e.printStackTrace();
        }
    }
    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = rootView.getContext().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
    public class GameInfo extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... params) {
            String add_prod_url = gvs.getServerIP()+"/gamelist.php";



            try {
                String userid=gvs.getUserid();
                String data = "userid=" + URLEncoder.encode(userid, "UTF-8");
                URL url = new URL(add_prod_url);
                HttpURLConnection http= (HttpURLConnection)url.openConnection();
                http.setRequestMethod("POST");
                http.setDoOutput(true);
                http.setFixedLengthStreamingMode(data.getBytes().length);
                http.setRequestProperty("Content - Type", "application / x - www - form - urlencoded");
                PrintWriter out = new PrintWriter(http.getOutputStream());
                out.print(data);
                out.close();

                InputStream is = http.getInputStream();
                String output = getStringFromInputStream(is);
                is.close();

                return output;

            }catch (MalformedURLException e)
            {
                Log.i("rosterinfo", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.i("rosterinfo", "ERROR to connect PHP : " + e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        private  String getStringFromInputStream(InputStream is) {

            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();

            String line;
            try {

                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return sb.toString();

        }
    }

}
