package com.hpp.puckbandit;

import android.provider.BaseColumns;

/**
 * Created by USER on 19-05-2016.
 */
public class TableData {
    public TableData()
    {

    }

    public  static abstract  class TableInfo implements BaseColumns
    {
        public static final String DATABASE_NAME = "puckbandit";
        public  static final String TABLE_NAME = "standings";
        public static final String id = "_id";
        public static final String ref_team_id = "ref_team_id";
        public static final String gp = "gp";
        public static final String win = "win";
        public static final String loss = "loss";
        public static final String otl = "otl";
        public static final String gf = "gf";
        public static final String ga = "ga";
        public static final String p = "p";
        public static final String rank = "rank";

    }
}
