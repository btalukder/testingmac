package com.hpp.puckbandit;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Created by Ankush_PC on 03-06-2016.
 */
public class StandingsFragment extends Fragment {


    private Button CGY,EDM,MTL, OTT, TOR,VAN,WPG;
    private Context ctx =getActivity();
    private static GlobalVariableStore gvs;
    public StandingsFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.standings_fragment_layout, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Standings Fragment");
        gvs=(GlobalVariableStore)getActivity().getApplicationContext();

        TableLayout tl = (TableLayout) rootView.findViewById(R.id.standingcontent);
        TableRow tempTableRow = new TableRow(getActivity());
        Button teamBtn = new Button(getActivity());
        TextView textViewnoGP =  new TextView(getActivity());
        TextView textViewnoW =  new TextView(getActivity());
        TextView textViewnoL =  new TextView(getActivity());
        TextView textViewnoOTL =  new TextView(getActivity());
        TextView textViewnoGF =  new TextView(getActivity());
        TextView textViewnoGA =  new TextView(getActivity());
        TextView textViewnoP =  new TextView(getActivity());

        /////////////////////////////////////////////////
        tempTableRow.getLayoutParams().width = TableLayout.LayoutParams.MATCH_PARENT;
        tempTableRow.getLayoutParams().height = TableLayout.LayoutParams.MATCH_PARENT;
        int padding =(int) rootView.getContext().getResources().getDimension(R.dimen.paddingStandingHead);
        tempTableRow.setPadding(0,0,0,padding);
        final int sdk = android.os.Build.VERSION.SDK_INT;

        tempTableRow.setBackgroundDrawable(rootView.getContext().getResources().getDrawable(R.drawable.topbottommargin));

        LinearLayout.LayoutParams params1=null;
        textViewnoGP.getLayoutParams().height=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoGP.getLayoutParams().width=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoGP.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoGP.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
         params1 =(LinearLayout.LayoutParams)textViewnoGP.getLayoutParams();
        params1.setMargins((int)(rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int) (rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int)    (rootView.getContext().getResources().getDisplayMetrics().density)*1,
                (int)     (rootView.getContext().getResources().getDisplayMetrics().density)*0);
        params1.weight = 1.0f;
        textViewnoGP.setLayoutParams(params1);




        textViewnoW.getLayoutParams().height=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoW.getLayoutParams().width=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoW.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoW.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        params1 =(LinearLayout.LayoutParams)textViewnoW.getLayoutParams();
        params1.setMargins((int)(rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int) (rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int)    (rootView.getContext().getResources().getDisplayMetrics().density)*1,
                (int)     (rootView.getContext().getResources().getDisplayMetrics().density)*0);
        params1.weight = 1.0f;
        textViewnoW.setLayoutParams(params1);



        textViewnoL.getLayoutParams().height=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoL.getLayoutParams().width=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoL.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoL.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        params1 =(LinearLayout.LayoutParams)textViewnoL.getLayoutParams();
        params1.setMargins((int)(rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int) (rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int)    (rootView.getContext().getResources().getDisplayMetrics().density)*1,
                (int)     (rootView.getContext().getResources().getDisplayMetrics().density)*0);
        params1.weight = 1.0f;
        textViewnoL.setLayoutParams(params1);

        textViewnoOTL.getLayoutParams().height=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoOTL.getLayoutParams().width=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoOTL.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoOTL.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        params1 =(LinearLayout.LayoutParams)textViewnoOTL.getLayoutParams();
        params1.setMargins((int)(rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int) (rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int)    (rootView.getContext().getResources().getDisplayMetrics().density)*1,
                (int)     (rootView.getContext().getResources().getDisplayMetrics().density)*0);
        params1.weight = 1.0f;
        textViewnoOTL.setLayoutParams(params1);


        textViewnoGF.getLayoutParams().height=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoGF.getLayoutParams().width=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoGF.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoGF.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        params1 =(LinearLayout.LayoutParams)textViewnoGF.getLayoutParams();
        params1.setMargins((int)(rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int) (rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int)    (rootView.getContext().getResources().getDisplayMetrics().density)*1,
                (int)     (rootView.getContext().getResources().getDisplayMetrics().density)*0);
        params1.weight = 1.0f;
        textViewnoGF.setLayoutParams(params1);


        textViewnoGA.getLayoutParams().height=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoGA.getLayoutParams().width=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoGA.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoGA.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        params1 =(LinearLayout.LayoutParams)textViewnoGA.getLayoutParams();
        params1.setMargins((int)(rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int) (rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int)    (rootView.getContext().getResources().getDisplayMetrics().density)*1,
                (int)     (rootView.getContext().getResources().getDisplayMetrics().density)*0);
        params1.weight = 1.0f;
        textViewnoGA.setLayoutParams(params1);

        textViewnoP.getLayoutParams().height=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoP.getLayoutParams().width=TableLayout.LayoutParams.WRAP_CONTENT;
        textViewnoP.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
        textViewnoP.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        params1 =(LinearLayout.LayoutParams)textViewnoP.getLayoutParams();
        params1.setMargins((int)(rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int) (rootView.getContext().getResources().getDisplayMetrics().density)*0,
                (int)    (rootView.getContext().getResources().getDisplayMetrics().density)*1,
                (int)     (rootView.getContext().getResources().getDisplayMetrics().density)*0);
        params1.weight = 1.0f;
        textViewnoP.setLayoutParams(params1);

        ////////////////////////////////////////////////

        DatabaseOperations standing = new DatabaseOperations(getActivity(),gvs);
        Cursor cr = standing.checkLocalDb(standing,"NA");
        cr.moveToFirst();
        do {
            String teamName = cr.getString(0);
            TextView temp ;
            switch (teamName)
            {
                case "MTL":
                    temp = (TextView) rootView.findViewById(R.id.mtlgp);
                    temp.setText(cr.getString(1));
                    temp = (TextView) rootView.findViewById(R.id.mtlw);
                    temp.setText(cr.getString(2));
                    temp = (TextView) rootView.findViewById(R.id.mtll);
                    temp.setText(cr.getString(3));
                    temp = (TextView) rootView.findViewById(R.id.mtlotl);
                    temp.setText(cr.getString(4));
                    temp = (TextView) rootView.findViewById(R.id.mtlgf);
                    temp.setText(cr.getString(5));
                    temp = (TextView) rootView.findViewById(R.id.mtlga);
                    temp.setText(cr.getString(6));
                    temp = (TextView) rootView.findViewById(R.id.mtlp);
                    temp.setText(cr.getString(7));
                    break;
                case "CGY":
                    temp = (TextView) rootView.findViewById(R.id.cgygp);
                    temp.setText(cr.getString(1));
                    temp = (TextView) rootView.findViewById(R.id.cgyw);
                    temp.setText(cr.getString(2));
                    temp = (TextView) rootView.findViewById(R.id.cgyl);
                    temp.setText(cr.getString(3));
                    temp = (TextView) rootView.findViewById(R.id.cgyotl);
                    temp.setText(cr.getString(4));
                    temp = (TextView) rootView.findViewById(R.id.cgygf);
                    temp.setText(cr.getString(5));
                    temp = (TextView) rootView.findViewById(R.id.cgyga);
                    temp.setText(cr.getString(6));
                    temp = (TextView) rootView.findViewById(R.id.cgyp);
                    temp.setText(cr.getString(7));
                    break;
                case "EDM":
                    temp = (TextView) rootView.findViewById(R.id.edmgp);
                    temp.setText(cr.getString(1));
                    temp = (TextView) rootView.findViewById(R.id.edmw);
                    temp.setText(cr.getString(2));
                    temp = (TextView) rootView.findViewById(R.id.edml);
                    temp.setText(cr.getString(3));
                    temp = (TextView) rootView.findViewById(R.id.edmotl);
                    temp.setText(cr.getString(4));
                    temp = (TextView) rootView.findViewById(R.id.edmgf);
                    temp.setText(cr.getString(5));
                    temp = (TextView) rootView.findViewById(R.id.edmga);
                    temp.setText(cr.getString(6));
                    temp = (TextView) rootView.findViewById(R.id.edmp);
                    temp.setText(cr.getString(7));
                    break;
                case "OTT":
                    temp = (TextView) rootView.findViewById(R.id.otlgp);
                    temp.setText(cr.getString(1));
                    temp = (TextView) rootView.findViewById(R.id.otlw);
                    temp.setText(cr.getString(2));
                    temp = (TextView) rootView.findViewById(R.id.otll);
                    temp.setText(cr.getString(3));
                    temp = (TextView) rootView.findViewById(R.id.otlotl);
                    temp.setText(cr.getString(4));
                    temp = (TextView) rootView.findViewById(R.id.otlgf);
                    temp.setText(cr.getString(5));
                    temp = (TextView) rootView.findViewById(R.id.otlga);
                    temp.setText(cr.getString(6));
                    temp = (TextView) rootView.findViewById(R.id.otlp);
                    temp.setText(cr.getString(7));
                    break;
                case "TOR":
                    temp = (TextView) rootView.findViewById(R.id.torgp);
                    temp.setText(cr.getString(1));
                    temp = (TextView) rootView.findViewById(R.id.torw);
                    temp.setText(cr.getString(2));
                    temp = (TextView) rootView.findViewById(R.id.torl);
                    temp.setText(cr.getString(3));
                    temp = (TextView) rootView.findViewById(R.id.torotl);
                    temp.setText(cr.getString(4));
                    temp = (TextView) rootView.findViewById(R.id.torgf);
                    temp.setText(cr.getString(5));
                    temp = (TextView) rootView.findViewById(R.id.torga);
                    temp.setText(cr.getString(6));
                    temp = (TextView) rootView.findViewById(R.id.torp);
                    temp.setText(cr.getString(7));
                    break;
                case "VAN":
                    temp = (TextView) rootView.findViewById(R.id.vangp);
                    temp.setText(cr.getString(1));
                    temp = (TextView) rootView.findViewById(R.id.vanw);
                    temp.setText(cr.getString(2));
                    temp = (TextView) rootView.findViewById(R.id.vanl);
                    temp.setText(cr.getString(3));
                    temp = (TextView) rootView.findViewById(R.id.vanotl);
                    temp.setText(cr.getString(4));
                    temp = (TextView) rootView.findViewById(R.id.vangf);
                    temp.setText(cr.getString(5));
                    temp = (TextView) rootView.findViewById(R.id.vanga);
                    temp.setText(cr.getString(6));
                    temp = (TextView) rootView.findViewById(R.id.vanp);
                    temp.setText(cr.getString(7));
                    break;
                case "WPG":
                    temp = (TextView) rootView.findViewById(R.id.wpggp);
                    temp.setText(cr.getString(1));
                    temp = (TextView) rootView.findViewById(R.id.wpgw);
                    temp.setText(cr.getString(2));
                    temp = (TextView) rootView.findViewById(R.id.wpgl);
                    temp.setText(cr.getString(3));
                    temp = (TextView) rootView.findViewById(R.id.wpgotl);
                    temp.setText(cr.getString(4));
                    temp = (TextView) rootView.findViewById(R.id.wpggf);
                    temp.setText(cr.getString(5));
                    temp = (TextView) rootView.findViewById(R.id.wpgga);
                    temp.setText(cr.getString(6));
                    temp = (TextView) rootView.findViewById(R.id.wpgp);
                    temp.setText(cr.getString(7));
                    break;

            }

        }while (cr.moveToNext());
        cr.close();

        CGY = (Button) rootView.findViewById(R.id.cgy);
        EDM = (Button) rootView.findViewById(R.id.edm);
        MTL = (Button) rootView.findViewById(R.id.mtl);
        OTT = (Button) rootView.findViewById(R.id.otlb);
        TOR =(Button) rootView.findViewById(R.id.tor);
        VAN =(Button) rootView.findViewById(R.id.van);
        WPG = (Button) rootView.findViewById(R.id.wpg);

        CGY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("CGY");
            }
        });
        EDM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("EDM");

            }
        });
        MTL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("MTL");
            }
        });
        OTT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("OTT");
            }
        });
        TOR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("TOR");
            }
        });
        VAN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRosterFragment("VAN");
            }
        });
        WPG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {loadRosterFragment("WPG");
            }
        });
        return rootView;
    }
public void loadRosterFragment(String teamname){


    Fragment fragment=null;



    // Add the fragment to the activity, pushing this transaction
    // on to the back stack.
    FragmentTransaction ft = getFragmentManager().beginTransaction();

    fragment = new TeamsFragment(teamname);
    ft.replace(R.id.frame, fragment);
    ft.addToBackStack("TeamsFragment");
    ft.commit();
}

}
